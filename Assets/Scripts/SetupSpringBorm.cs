﻿using UnityEngine;
using System.Collections;

using UnityChan;

public class SetupSpringBorm : MonoBehaviour
{
    int SMIndex;
    int AllChildCount;
    SpringManager SM;

    // Use this for initialization
    void Start ()
    {
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    [ContextMenu ("SetupSpringBorm")]
    void Setup ()
    {
        SMIndex = 0;
        SM = gameObject.GetComponent<SpringManager> ();

        if (SM == null) {
            SM = gameObject.AddComponent<SpringManager> ();        
        }

        AllChildCount = 0;
        GetChildCount (transform);

        ChildSearch (gameObject, null);
    }

    void ChildSearch (GameObject parent, SpringBone parentSB)
    {
        foreach (Transform child in parent.transform) {
            if (child != null) {
                SpringBone SBCom = child.gameObject.GetComponent<SpringBone> ();
                if (SBCom == null) {
                    SBCom = child.gameObject.AddComponent<SpringBone> ();
                }

                if (parentSB != null) {
                    parentSB.child = child;
                }

                SM.springBones [SMIndex] = SBCom;
                SMIndex++;

                if (child.childCount == 0) {
                    SBCom.child = parent.transform;
                } else {
                    ChildSearch (child.gameObject, SBCom);
                }

            }                
        }
    }

    void GetChildCount (Transform parent)
    {
        foreach (Transform child in parent) {
            AllChildCount++;
            GetChildCount (child);
        }
    }
}