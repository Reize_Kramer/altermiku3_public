﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof(Rigidbody))]

public class StageProgress : MonoBehaviour
{
    public float Speed;
    public float targetSpeed;

    //ステージ進行速度
    Animator playerAnimator;
    GameObject Player;
    //Rigidbody rb;

    // Use this for initialization
    void Start ()
    {
        Player = GameObject.FindGameObjectWithTag ("Player");
        playerAnimator = Player.GetComponent<Animator> ();
        //rb = GetComponent<Rigidbody>();

        //前進
        //rb.velocity = new Vector3(0.0f, 0.0f, Speed);

    }
	
    // Update is called once per frame
    void Update ()
    {
        if (Speed > 1.0f) {
            //プレイヤーに前進モーションをとらせる。
            if (Player.activeSelf) {
                playerAnimator.SetInteger("VelocityZ", 1);
            }
        } else {
            //プレイヤーに待機モーションをとらせる
            if (Player.activeSelf) {
                playerAnimator.SetInteger("VelocityZ", 0);
                Player.transform.localRotation = Quaternion.Slerp(
                    Player.transform.localRotation,
                    Quaternion.Euler(0.0f, Player.transform.localEulerAngles.y, Player.transform.localEulerAngles.z),
                    Time.deltaTime * 1
                );
            }
        }

        //目標速度に近づける（目標速度はほかのスクリプトから操作される
        Speed = Mathf.Lerp(Speed, targetSpeed, Time.deltaTime * 2);

    }

    void FixedUpdate ()
    {
        //前進検証
        //rb.MovePosition(new Vector3(transform.position.x, transform.position.y, transform.position.z + Time.deltaTime * Speed));
        //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + Time.deltaTime * Speed);
        transform.Translate (Vector3.forward * Speed * Time.deltaTime);

    }
}
