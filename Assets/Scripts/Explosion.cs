﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
    public GameObject Particle;
    //パーティクルシステムのプレファブ
    GameObject instance;
    //生成したプレファブ
    ParticleSystem PS;
    //コンポーネント
    public Vector3 ParticleScale = Vector3.zero;
    //パーティクルをスケーリングする場合
    public bool isLocalSpace = false;

    AudioSource Audio;
    //爆発音
    
    // Use this for initialization
    void Start ()
    {
        instance = Instantiate (Particle, transform.position, transform.rotation) as GameObject;
        instance.transform.parent = transform;
        PS = instance.GetComponent<ParticleSystem> ();
        Audio = GetComponent<AudioSource> ();

        if (ParticleScale != Vector3.zero) {
            PS.scalingMode = ParticleSystemScalingMode.Local;
            instance.transform.localScale = ParticleScale;
        }

        //パーティクルをローカル座標に追従させたい場合
        if (isLocalSpace) {
            PS.simulationSpace = ParticleSystemSimulationSpace.Local;
        }
    }
	
    // Update is called once per frame
    void Update ()
    {
        //パーティクルとサウンドの再生が終わったらDestroy
        if (PS.isPlaying == false && Audio.isPlaying == false) {
            Destroy (gameObject);
        }
    }
}
