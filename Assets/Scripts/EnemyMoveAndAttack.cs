﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMoveAndAttack : MonoBehaviour
{
    Vector3 WaypointPos;
    Vector3 WaypointRotate;
    public GameObject Waypoint;
    public float fMoveSpeed = 1.0f;
    public float RotateSpeed = 1.0f;
    GameObject PlayerCenter;
    public GameObject Shot;
    public float Sec = 3.0f;
    //発射から着弾までの時間
    public float ShotInterval = 5.0f;
    //弾の発射間隔

    delegate void MoveObject ();

    MoveObject deligateMoveObject;

    bool AddedMoveRotateToPlayer = false;
    bool Attacked = false;

    public GameObject AlertMerkerPrefab;
    GameObject AlertMerker;

    public static List<GameObject> EnemyShotToPlayerPools;

    GameObject EnemyManagerObj;

    // Use this for initialization
    void Start ()
    {
        PlayerCenter = GameObject.FindGameObjectWithTag ("PlayerCenter");
        EnemyManagerObj = GameObject.FindGameObjectWithTag ("EnemyManager");

        /*
        if (EnemyShotToPlayerPools == null) {
            EnemyShotToPlayerPools = new List<GameObject>();
        }
        */

        if (AlertMarkerGUI.AlertMarkerPools == null) {
            AlertMarkerGUI.AlertMarkerPools = new List<GameObject>();
        }

        //デリゲートに最初のメソッドを設定
        deligateMoveObject = MoveToWaypoint;

        //もし呼び元からWaypointが指定されない場合は、移動しない（自分を目標にする）
        if (Waypoint == null) {
            Waypoint = gameObject;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        deligateMoveObject ();
    }

    void MoveToWaypoint ()
    {
        //目標地点に移動
        transform.position = Vector3.Lerp (transform.position, Waypoint.transform.position, fMoveSpeed * Time.deltaTime);

        //目標地点に近づいたら、プレイヤーの方を向く
        if (AddedMoveRotateToPlayer == false
            && Vector3.SqrMagnitude (transform.position - Waypoint.transform.position) < 80.0f) {
            deligateMoveObject += MoveRotateToPlayer;
            AddedMoveRotateToPlayer = true;
        }
    }

    void MoveRotateToPlayer ()
    {
        //プレイヤーの方を向く
        Vector3 relativePos = PlayerCenter.transform.position - transform.position;
        Quaternion toPlayerRotation = Quaternion.LookRotation (relativePos);
        transform.rotation = Quaternion.Slerp (transform.rotation, toPlayerRotation, RotateSpeed * Time.deltaTime);

        //旋回がある程度完了したら攻撃する
        if (Attacked == false
            && Quaternion.Angle (transform.rotation, toPlayerRotation) < 20.0f) {
            StartCoroutine (ToPlayerShot ());
            Attacked = true;
        }
    }

    IEnumerator ToPlayerShot ()
    {
        Vector3 From, To; //発射地点と目標地点
        GameObject ShotObj; //生成した弾
        Vector3 shotSpeed; //弾速
        Rigidbody rb; //弾のRigidbody
        
        //撃破されるまで弾を撃ち続ける
        while (gameObject != null) {
            //現在位置を発射位置として取得
            From = transform.position;

            //プレイヤーの現在位置を目標として取得
            To = PlayerCenter.transform.position;

            //弾速を算出
            shotSpeed = (To - From) / Sec;
            
            //生成
            //オブジェクトプールリストに使用済みの弾があれば利用
            //なければ生成
            if (EnemyMoveAndAttack.EnemyShotToPlayerPools.Count != 0) {
                ShotObj = EnemyShotToPlayerPools [0];
                EnemyShotToPlayerPools.RemoveAt (0);
                ShotObj.SetActive (true);
                ShotObj.transform.position = From;
                ShotObj.transform.rotation = transform.rotation;
            } else {
                ShotObj = Instantiate (Shot, From, transform.rotation) as GameObject;
            }

            ShotObj.transform.parent = EnemyManagerObj.transform;
            EnemyShot EnemyShotScript = ShotObj.GetComponent<EnemyShot> ();
            EnemyShotScript.LifeTime = Sec + 2.0f;
            rb = ShotObj.GetComponent<Rigidbody> ();
            rb.velocity = shotSpeed;

            //攻撃アラートマーカーを表示
            //オブジェクトプールリストに使用済みのマーカーがあれば利用
            //なければ生成
            GameObject AlertMarkerInstance;
            if (AlertMarkerGUI.AlertMarkerPools.Count != 0) {
                AlertMarkerInstance = AlertMarkerGUI.AlertMarkerPools [0];
                AlertMarkerGUI.AlertMarkerPools.RemoveAt (0);                
                AlertMarkerInstance.transform.position = To;
                AlertMarkerInstance.transform.rotation = Quaternion.identity;
            } else {
                AlertMarkerInstance = Instantiate (AlertMerkerPrefab, To, Quaternion.identity) as GameObject;
            }

            AlertMarkerInstance.transform.parent = EnemyManagerObj.transform;
            AlertMarkerGUI AlertMarkerScript = AlertMarkerInstance.GetComponent<AlertMarkerGUI> ();
            AlertMarkerScript.LandingTime = Sec;
            AlertMarkerScript.ShotPointPos = From;
            AlertMarkerScript.TargetPos = To;

            AlertMarkerInstance.SetActive (true);

            yield return new WaitForSeconds (ShotInterval);
        }
    }

    void OnDestroy ()
    {
        //自分が消えるときにWaypointも消す
        Destroy (Waypoint);
    }
}
