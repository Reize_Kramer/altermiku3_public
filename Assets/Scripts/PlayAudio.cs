﻿using UnityEngine;
using System.Collections;

public class PlayAudio : MonoBehaviour
{
    AudioSource AS;

    // Use this for initialization
    void Start ()
    {
        AS = GetComponent<AudioSource> ();
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    void PlayAudioSource ()
    {
        AS.Play ();
    }
}
