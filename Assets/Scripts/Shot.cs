﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {
    Rigidbody rb;
    public GameObject target;
    Transform tfTarget;
    float shotSpeed = 50.0f;
    float time = 0.2f;
    bool bHit;
    float DURATION = 5.0f;
    public float HormingPower = 4.0f;
    GameObject BulletMesh;

    void OnEnable() {
        bHit = false;
        time = 0.2f;

        //オブジェクトプールから再生成すると、前の弾の加速度が残っているので
        //リセットする
        if (rb != null) {
            rb.velocity = Vector3.zero;
        }

        if (BulletMesh != null) {
            BulletMesh.SetActive(true);
        }
    }

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        BulletMesh = transform.FindChild("Sphere").gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (bHit == false && target != null) {
            //targetに向かって誘導
            //時間経過とターゲットとの距離から誘導力を決める
            if (time >= 0f) {
                Quaternion targetDirection = Quaternion.LookRotation(target.transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetDirection, time * HormingPower / 100);
            }
        }
        rb.velocity = (transform.forward * shotSpeed);

        //発射後一定時間経過していたら弾体を消して加速度をリセット
        //さらに一定時間経過でオブジェクトをInactive
        if( time >= DURATION) {
            BulletMesh.SetActive(false);
            rb.velocity = Vector3.zero;

            if (time >= DURATION + 1) {
                SetInactive();
            }
        }
    }

    void OnTriggerEnter(Collider col) {
        //ターゲットに命中したら追尾を切る
        if(col.gameObject == target) {
            bHit = true;

            //命中処理
            IEnemy enemyScript = target.GetComponent(typeof(IEnemy)) as IEnemy;
            enemyScript.hit();

            //命中後、一定時間でオブジェクトを無効化
            time = DURATION - 1.5f;
        }
    }

    //無効化してオブジェクトプールに追加
    void SetInactive() {
        AttackSystem.ShotsPool.Add(gameObject);
        gameObject.SetActive(false);
    }
}
