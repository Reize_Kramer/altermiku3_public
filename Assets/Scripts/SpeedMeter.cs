﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeedMeter : MonoBehaviour {
    GameObject Player;
    Text comText;
    Vector3 vPrevFlamePos;
	float fPrevSpeed;
    float timer = 0;

	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag ("Player");
        comText = GetComponent<Text> ();
        vPrevFlamePos = Player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        Vector3 vCurrentFramePos = Player.transform.position;
        if (timer >= 0.1f) {
            //プレイヤーの速度をテキストに表示
            float fDeltaMove = Vector3.Distance (vPrevFlamePos, vCurrentFramePos);
            float fSpeed = fDeltaMove / Time.deltaTime;
			fSpeed = Mathf.Lerp(fPrevSpeed, fSpeed, 20.0f);
            comText.text = string.Format ("Speed:\n{0:000.00}", fSpeed * 20);
			fPrevSpeed = fSpeed;
            timer = 0;

        }
        vPrevFlamePos = vCurrentFramePos;
		      	
     

    }
}
