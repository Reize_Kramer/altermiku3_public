﻿using UnityEngine;
using System.Collections;

public class MyPlayerControl : MonoBehaviour {
    public float AcceleSpeed;
    //Rigidbody rb;
    CharacterController cc;
    //Animator ar;
    //float fBoost = 0.0f;
    //float BoostAccel = 20.0f;

	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody>();
        cc = GetComponent<CharacterController>();
        //ar = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        //旋回
        if( Input.GetAxis("Horizontal") != 0) {
            transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * Time.deltaTime * 100, 0),Space.World);
        }

        //見上げ・見下げ
        if( Input.GetAxis("Vertical") != 0) {
            
        }

        //前進・後退
        if( Input.GetAxis("Forward_Back") != 0.0f) {
            Vector3 vDir = Camera.main.transform.forward;
            //カメラ位置を考慮して角度を調整
            cc.Move(vDir.normalized * Input.GetAxis("Forward_Back") * Time.deltaTime * AcceleSpeed);
        }

        //左右移動
        if( Input.GetAxis("Strafe") != 0.0f) {
            Vector3 MoveAmount = (transform.right * Input.GetAxis("Strafe") * Time.deltaTime * AcceleSpeed);
            cc.Move(MoveAmount);
            //カメラが追従しないとカメラを中心に回ってしまうので動かす
            Camera.main.transform.position += MoveAmount;
        }

        //モーション制御
        Quaternion rotateX;
        if( Input.GetAxis("Forward_Back") > 0.0f) {
            //ar.SetInteger("VelocityZ", 1);
            rotateX = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(60.0f, transform.localEulerAngles.y, transform.localEulerAngles.z), Time.deltaTime * 1);
        } else if ( Input.GetAxis("Forward_Back") < 0.0f) {
            rotateX = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(-30.0f, transform.localEulerAngles.y, transform.localEulerAngles.z), Time.deltaTime * 1);
        } else {
            //ar.SetInteger("VelocityZ", 0);
            rotateX = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, transform.localEulerAngles.y, transform.localEulerAngles.z), Time.deltaTime * 1);
        }
        transform.localRotation = rotateX;
    }
}
