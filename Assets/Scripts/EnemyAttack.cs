﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {
    public GameObject Player;
    public bool doAttack;
    public GameObject Shot;

    
    public float Sec = 3.0f; //発射から着弾までの時間
    

    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        //デバッグ用にスペースキーで敵が弾を撃つように
        if ( Input.GetButtonDown("Jump") ) {
            StartCoroutine(ToPlayerShot());
        }

	}

    IEnumerator ToPlayerShot() {
        Vector3 From, To; //発射地点と目標地点
        GameObject ShotObj; //生成した弾
        Vector3 shotSpeed; //弾速
        Rigidbody rb; //弾のRigidbody

        //現在位置を発射位置として取得
        From = transform.position;

        //プレイヤーの現在位置を目標として取得
        To = Player.transform.position;

        //弾速を算出
        shotSpeed = (To - From) / Sec;

        //生成
        ShotObj = Instantiate(Shot, From, transform.rotation) as GameObject;
        rb = ShotObj.GetComponent<Rigidbody>();
        rb.velocity = shotSpeed;
        
        yield return new WaitForSeconds(Sec);
        
        //弾が外れていたら５秒後に弾を消去
        if ( ShotObj != null) {
            Destroy(ShotObj, 5.0f);
        }        
    }
}
