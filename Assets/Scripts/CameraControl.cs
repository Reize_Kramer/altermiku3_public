﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
    //Vector3 DefaultPosition;
    GameObject goCameraTarget;
    GameObject goRotationAdjust;
    Transform tfPlayerHead;
    public float minDist; //カメラの最低高度（地面にもぐらないように）

    public float lookMargin; //カメラ揺れ対策のアソビ
    public float trackSpeed; //カメラの追従速度

	// Use this for initialization
	void Start () {
        //複数モデルの使い分けをできるように、スクリプトで参照をとりにいく
        goRotationAdjust = GameObject.Find("RotationAdjust");
        goCameraTarget = GameObject.Find("CameraTarget");

        tfPlayerHead = goCameraTarget.transform.parent;
        //DefaultPosition = transform.position - tfPlayerHead.position;
    }

    // Update is called once per frame
    void Update() {
        //キャラのローカル傾きを相殺する
        Vector3 adjustRotation = goRotationAdjust.transform.parent.eulerAngles;
        float adjustX = adjustRotation.x;
        //0～360から-180～180の単位に変換
        adjustX = (adjustX > 180.0f) ? adjustX - 360.0f : adjustX;
        //反転値
        adjustX -= adjustX;
        //再変換
        adjustX = (adjustX < 0) ? adjustX + 360.0f : adjustX;
        adjustRotation.x = adjustX;
        goRotationAdjust.transform.eulerAngles = adjustRotation;

        //上下入力でカメラの視点を上げ下げ
        Vector3 rotationAmount = Vector3.right;
        if (Input.GetAxis("Vertical") != 0.0f) {
            rotationAmount = new Vector3(Input.GetAxis("Vertical"), 0.0f, 0.0f) * Time.deltaTime * 100;
            tfPlayerHead.Rotate(rotationAmount);
        }

        //視点が地面にめり込む場合は回転をキャンセル
        //if (goCameraTarget.transform.position.y < 0) {
        //    tfPlayerHead.Rotate(Vector3.right * Time.deltaTime);
        //}

        //角度制限
        Vector3 cameraAngle = tfPlayerHead.localEulerAngles;
        float rotationX = cameraAngle.x;
        //0～360から-180～180の単位に変換
        rotationX = (rotationX > 180.0f) ? rotationX - 360.0f : rotationX;
        //角度制限
        rotationX = Mathf.Clamp(rotationX, -70.0f, 70.0f);
        //再変換
        rotationX = (rotationX < 0) ? rotationX + 360.0f : rotationX;
        tfPlayerHead.localEulerAngles = new Vector3(rotationX, cameraAngle.y, cameraAngle.z);


        //メインカメラ追従
        //キャラに近づきすぎたら高速で距離をとる
        float sqrDistance = Vector3.SqrMagnitude(transform.position - tfPlayerHead.position);
        float cameraTrackSpeed = trackSpeed;
        if (sqrDistance <= 1) {
            cameraTrackSpeed = cameraTrackSpeed * 5;
        }

        Vector3 moveVector = Vector3.Lerp(transform.position, goCameraTarget.transform.position, Time.deltaTime * cameraTrackSpeed);

        //カメラの最低高度
        //moveVector.y = Mathf.Clamp(moveVector.y, minDist, moveVector.y);
        
        //移動距離が少ない場合は動かさない（画面揺れ対策）
        //if (moveVector.sqrMagnitude > 1.0f) {
            transform.position = moveVector;
        //}

        //カメラの向きはキャラの頭に
        //移動距離が少ない場合は動かさない（画面揺れ対策）
        Quaternion lookPlayer = Quaternion.LookRotation(tfPlayerHead.position - transform.position);
        if (Mathf.Abs(Quaternion.Angle(transform.rotation, lookPlayer)) > lookMargin) { 
            transform.rotation = Quaternion.Slerp(transform.rotation, lookPlayer, Time.deltaTime * 5);
        }        
                
	}
}
