﻿using UnityEngine;
using System.Collections;

public class BackCamera : MonoBehaviour {
    //背面追従カメラの制御

    //CameraVibration CV;
    GameObject Player;
    public Vector3 LookPosFar;
    Vector3 CurrentSpeed;    
    public GameObject LookPos; //視点の目標とするオブジェクト
    public float SmoothSpeed;

    //float DefaultFar;
    public float ForwardBackSpeed;
    //Camera CameraCom;
    public float FOVRate;
    float FOVCurVel;
    public float FOVMoveTime;
    public float FOVMaxSpeed;

    //シーン開始時のカメラ位置
    public Vector3 StartPosition;
    public Vector3 StartRotation;

	// Use this for initialization
	void Start () {
        //CV = GetComponent<CameraVibration>();
        Player = GameObject.FindGameObjectWithTag("Player");

        Vector3 targetPos = Player.transform.localPosition;
        targetPos += LookPosFar;
        LookPos.transform.localPosition = targetPos;

        //初期位置を設定（演出などの関係で、DefaultPositionとは異なる）
        transform.position = StartPosition;
        transform.rotation = Quaternion.Euler(StartRotation);

        //DefaultFar = Vector3.SqrMagnitude(CV.DefaultPosition);

        //CameraCom = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        //急にカメラが動くと酔うので、SmoothDampを使ってマイルドにカメラを向ける
        Vector3 targetPos = Player.transform.localPosition;
        targetPos += LookPosFar;

        //縦方向のみ、視点移動量を増やす
        //targetPos.y += Player.transform.localPosition.y;

        LookPos.transform.localPosition = Vector3.SmoothDamp(LookPos.transform.localPosition, targetPos, ref CurrentSpeed, SmoothSpeed);
        transform.LookAt(LookPos.transform.position);

        ////プレイヤーとの位置関係を保つようにパンする
        //float DifPos = Vector3.SqrMagnitude(Player.transform.localPosition - transform.localPosition);
        //Debug.Log("DifPos:" + DifPos);
        //if (DifPos > DefaultFar) {
        //    CameraCom.fieldOfView = Mathf.SmoothDamp(CameraCom.fieldOfView, 60 + (DifPos - DefaultFar) * FOVRate, ref FOVCurVel, FOVMoveTime, FOVMaxSpeed);
        //} else if (DifPos < DefaultFar) {
        //    CameraCom.fieldOfView = Mathf.SmoothDamp(CameraCom.fieldOfView, 60 - (DifPos - DefaultFar) * FOVRate, ref FOVCurVel, FOVMoveTime, FOVMaxSpeed);
        //}
	}
}