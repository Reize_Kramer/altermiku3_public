﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class RetryTouchWaiting : MonoBehaviour {
    public Image ClearMessage;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //クリアメッセージの表示が完了したら、イベントを登録する。
        if (ClearMessage.color.a >= 0.9f) {
            Debug.Log("Added event Retry");
            AttachEvent();
        }
        
	}

    void AttachEvent() {
        //タッチでリトライするイベントを登録する
        InputController.TouchEnd += Retry;
    }

    void Retry(Vector3 Buf) {
        SceneManager.LoadScene(0);
    }
}
