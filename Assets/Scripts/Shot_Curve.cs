﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Shot_Curve : MonoBehaviour {
    Rigidbody rb;
    public GameObject target;
    Transform tfTarget;
    float shotSpeed = 50.0f;
    float time = 0.2f;
    float hitTime = 1.0f;
    bool bHit;
    bool PlayTween;

    public Vector3[] Positions;

    void OnEnable() {
        //生成後５秒で無効化
        Invoke("SetInactive", 5.0f);
        bHit = false;
        time = 0.2f;

        //オブジェクトプールから再生成すると、前の弾の加速度が残っているので
        //リセットする
        if (rb != null) {
            rb.velocity = Vector3.zero;
        }

        PlayTween = true;


    }

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        if (PlayTween) {
            //軌跡の点を管理する関数をコールバックで設定
            HomingLaser HomingLaserCom = GetComponent<HomingLaser>();
            if (HomingLaserCom != null) {
                transform.DOLocalPath(Positions, hitTime, PathType.CatmullRom, PathMode.Full3D, 2).OnUpdate(HomingLaserCom.addPoint).SetUpdate(UpdateType.Normal);
            }

            PlayTween = false;
        }
        /*
        if (bHit == false && target != null) {
            //targetに向かって誘導
            //時間経過とターゲットとの距離から誘導力を決める
            time += Time.deltaTime;
            float dif = Vector3.SqrMagnitude(transform.position - target.transform.position);
            
            float HomingVel = Mathf.Clamp((100 - dif) / 100,0 ,0.8f) + Mathf.Clamp(time * 0.1f, 0, 0.1f);


            //発射後一定時間は直進
            if (time >= 0f) {
                Quaternion targetDirection = Quaternion.LookRotation(target.transform.position - transform.position);

                //Debug.DrawRay(transform.position, target.transform.position - transform.position);
                //Debug.DrawRay(transform.position, transform.forward);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetDirection, HomingVel);
            }
        }
        rb.velocity = (transform.forward * shotSpeed);
        */
    }

    void OnTriggerEnter(Collider col) {
        //ターゲットに命中したら追尾を切る
        if(col.gameObject == target) {
            bHit = true;
            //TrailRenderer renderer = GetComponent<TrailRenderer>();
            //renderer.enabled = false;

            //命中処理
            IEnemy enemyScript = target.GetComponent(typeof(IEnemy)) as IEnemy;
            enemyScript.hit();
        }
    }

    //無効化してオブジェクトプールに追加
    void SetInactive() {
        AttackSystem.ShotsPool.Add(gameObject);
        gameObject.SetActive(false);
    }
}
