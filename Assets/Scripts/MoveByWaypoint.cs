﻿using UnityEngine;
using System.Collections;

public class MoveByWaypoint : MonoBehaviour {
    Vector3 WaypointPos;
    Vector3 WaypointRotate;
    public GameObject Waypoint;
    public float fMoveSpeed = 1.0f;


    delegate void MoveObject();
    MoveObject deligateMoveObject;

	// Use this for initialization
	void Start () {
        //デリゲートに最初のメソッドを設定
        deligateMoveObject = MoveToWaypoint;

        //もしWaypointが指定されない場合は、移動しない（自分を目標にする）
        if( Waypoint == null) {
            Waypoint = gameObject;
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        deligateMoveObject();
	}

    void MoveToWaypoint() {
        //目標地点に移動
        transform.position = Vector3.Lerp(transform.position, Waypoint.transform.position, fMoveSpeed * Time.deltaTime);
    }

    void MoveRotateToPlayer() {

    }

    void OnDestroy() {
        //自分が消えるときにWaypointも消す
        Destroy(Waypoint);
    }
}
