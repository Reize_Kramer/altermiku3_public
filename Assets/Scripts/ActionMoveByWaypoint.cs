﻿using UnityEngine;
using System.Collections;
using System;

public class ActionMoveByWaypoint : MonoBehaviour,IEnemyActionPattern {
    Vector3 CurVel;
    public GameObject Waypoint;
    public float fMoveMaxSpeed = 10.0f;
    public float fMoveTime = 0.1f;
    
    // Use this for initialization
    void Start () {
        CurVel = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Execute(GameObject go) {
        Debug.Log("Execute:MoveByWaypoint");
        //目標地点に移動
        go.transform.localPosition = Vector3.SmoothDamp(go.transform.localPosition, Waypoint.transform.localPosition, ref CurVel, 0.1f, fMoveMaxSpeed);
    }

    public bool EndAction(GameObject go) {
        return false;
    }
}