﻿using UnityEngine;
using System.Collections;

public class Gizmo : MonoBehaviour {
    public Mesh mesh; //ギズモ用メッシュ
    //public Color GizmoColor; //ギズモの色

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmos() {
        //Gizmos.color = GizmoColor;
        if (mesh != null) {
            Gizmos.DrawMesh(mesh, transform.position, transform.rotation, transform.localScale);
        } else {
            Gizmos.DrawSphere(transform.position, 1.0f);
        }
    }
}
