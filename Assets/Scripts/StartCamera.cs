﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking.Match;

public class StartCamera : MonoBehaviour
{
    GameObject Player;
    CameraVibration CV;
    Canvas CanvasCom;

    StageProgress StageProgressScript;

    // Use this for initialization
    void Start ()
    {
        //メインカメラのVibrationコンポーネントを一旦切る
        CV = Camera.main.gameObject.GetComponent<CameraVibration> ();
        if (CV != null) {
            CV.enabled = false;
        }

        Player = GameObject.FindGameObjectWithTag ("Player");

        //GUIを非表示にする
        GameObject GUICV = GameObject.FindGameObjectWithTag("GUICanvas");
        CanvasCom = GUICV.GetComponentInParent<Canvas>();
        if(CanvasCom != null) {
            CanvasCom.enabled = false;
        } else {
            Debug.LogError("Canvas in GUICanvas component not found.");
        }

        StageProgressScript = Player.transform.parent.GetComponent<StageProgress>();
        //ステージ速度を高速に設定する
        StageProgressScript.targetSpeed = 20.0f;

    }

    // Update is called once per frame
    void Update ()
    {

        transform.LookAt (Player.transform);



        //プレイヤーが追い越していったらメインカメラに戻す

        if (transform.position.z < Player.transform.position.z) {
            CV.enabled = true;
            gameObject.SetActive (false);

            //GUIを表示する
            if(CanvasCom != null) {
                CanvasCom.enabled = true;
            }

            //速度を落とす
            StageProgressScript.targetSpeed = 10.0f;
        }
    }
}
