﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour {
    public GameObject prefab;
    public GameObject Waypoint;

	// Use this for initialization
	void Start () {
        GameObject emittedObject = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
        //エミッターの親を、生成した敵の親に設定
        emittedObject.transform.parent = transform.parent;

        //生成した敵がMoveByWaypointコンポーネントを持つ場合、
        //Waypointを渡してやる
        MoveByWaypoint scMoveByWaypoint = emittedObject.GetComponent<MoveByWaypoint>();

        if(scMoveByWaypoint)
        if( scMoveByWaypoint != null) {
            scMoveByWaypoint.Waypoint = Waypoint;

            //Waypointの親子関係をエミッターの親に譲る
            Waypoint.transform.parent = transform.parent;
        }

        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
