﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] Waves;
    GameObject currentWave;
    public int WaveIndex = 0;
    public bool doEnemyGenerate = true;

    //敵残数管理オブジェクト（暫定実装）
    GameObject EnemyForcesManager;
    Slider EnemyForcesGauge;
    public bool EnableEnemyForcesManager;

    public int Level;

    public bool doListLoop; //敵をすべて倒したらループしたい場合（主にテスト用）

    // Use this for initialization
    void Start ()
    {
        currentWave = null;

        //敵残数管理オブジェクト（暫定実装）
        if (EnableEnemyForcesManager) {
            EnemyForcesManager = GameObject.Find("EnemyForcesManager");
            if (EnemyForcesManager != null) {
                EnemyForcesGauge = EnemyForcesManager.GetComponent<Slider>();
            }
        } else {
            EnemyForcesManager = null;
            EnemyForcesGauge = null;
        }
    }
	
    // Update is called once per frame
    void Update ()
    {
        if (doEnemyGenerate == true) {
            //Waveが存在しないなら、新しいWaveを生成する
            if (currentWave == null) {
                currentWave = Instantiate (Waves [WaveIndex], transform.position, transform.rotation) as GameObject;
                currentWave.transform.parent = transform;
                currentWave.SetActive(true);
               
                //WaveIndexを増加
                WaveIndex++;

            } else {
                //Waveの敵が全滅していたら、次のWaveを出す
                bool hasEnemy = false;
                foreach (Transform child in currentWave.transform) {
                    if (child.tag == "Enemy") {
                        hasEnemy = true;

                        //レベル管理
                        if (EnemyForcesGauge != null) {
                            Level = Mathf.FloorToInt ((EnemyForcesGauge.maxValue - EnemyForcesGauge.value) / 10) + 1;

                            //レベルに応じて敵の移動速度と連射速度をアップ
                            var EnemyScript = child.GetComponent<EnemyMoveAndAttack> ();
                            if (EnemyScript != null) {
                                EnemyScript.fMoveSpeed = Level;
                                EnemyScript.RotateSpeed = Level;
                                EnemyScript.ShotInterval = 3.0f / Level;
                            }
                        }
                    }
                }

                if (hasEnemy == false) {
                    //ゲーム終了条件を満たしているなら、敵生成を止めてクリア処理をする。
                    //すべてのWaveを消化していていたら終了
                    if (WaveIndex >= Waves.Length || Waves[WaveIndex] == null) {
                        //もしループ設定の場合はリストをループ
                        if (doListLoop) {
                            WaveIndex = 0;
                        } else {
                            StageClearMethod();
                        }
                    }
                        
                    if (EnemyForcesGauge != null) {
                        if (EnemyForcesGauge.value <= 0) {
                            StageClearMethod ();
                        }
                    }
                    currentWave = null;
                    //Destroy(currentWave);
                }
            }            
        }
    }

    void StageClearMethod ()
    {
        doEnemyGenerate = false;

        var GC = GameObject.FindGameObjectWithTag ("GameController");
        var ClearScript = GC.GetComponent<StageClear> ();
        ClearScript.isClear = true;
    }

    //敵弾をすべて消去するメソッド
    //DamageController等から使用
    public void EnemyShotsReset() {
        //EnemyShotタグを持つオブジェクトをすべてピックアップ
        GameObject[] EnemyShots = GameObject.FindGameObjectsWithTag("EnemyShot");
        foreach(var Shotobj in EnemyShots){
            //アクティブなオブジェクトしかFindには引っかからないはずだけど、一応チェック
            if(Shotobj.activeInHierarchy == true) {
                //弾を無効
                Shotobj.GetComponent<EnemyShot>().Disappear();
            }
        }
    }
}
