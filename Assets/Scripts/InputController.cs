﻿using UnityEngine;
using System.Collections;
using System;
using GodTouches;

public class InputController : MonoBehaviour {
    //入力を総括するコンポーネント
    //入力を必要とするコンポーネントはこれのスタティックメンバにイベントを登録して
    //反応を受け取る
    public static event Action<Vector3> TouchDown;
    public static event Action<Vector3> TouchMove;
    public static event Action<Vector3> TouchEnd;
    public static event Action<Vector3> Flick;

    //フリック処理の判定用
    Vector3 StartPosition = Vector3.zero;
    float TouchTime;
    public static float FlickThresholdTime = 0.2f;
    public float FlickThresholdDisSqr = 100.0f;

    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GodPhase TouchPhase = GodTouch.GetPhase();

        //フリック
        if (Flick != null) {
            Vector3 FlickVector;
            if (DetectFlick(out FlickVector)) {
                Flick(FlickVector);
            }
        }

        //タッチダウン
        if ( TouchDown != null ) {
            if (TouchPhase == GodPhase.Began) {
                TouchDown(GodTouch.GetPosition());                
            }
        }

        //タッチムーブ
        if ( TouchMove != null ) {
            if (TouchPhase == GodPhase.Moved) {
                TouchMove(GodTouch.GetPosition());
            }
        }

        //タッチエンド
        if ( TouchEnd != null ) {
            if (TouchPhase == GodPhase.Ended) {
                TouchEnd(GodTouch.GetPosition());
            }
        }
    }

    //フリック入力からQBする方向を判定する関数
    public bool DetectFlick(out Vector3 FlickVector) {
        bool isFlick = false;
        FlickVector = Vector3.zero;


        //タップを取得
        GodPhase TouchPhase = GodTouch.GetPhase();

        //タップ開始時
        if (TouchPhase == GodPhase.Began) {
            StartPosition = GodTouch.GetPosition();
            TouchTime = 0.0f;
        } else if (TouchPhase == GodPhase.Ended) {
            //タップ終了時
            //タッチ時間が一定以下のみフリック扱い
            if (TouchTime >= FlickThresholdTime) {
                TouchTime = 0.0f;
                return false;
            }

            //移動量が一定以上なら
            Vector3 EndPosition = GodTouch.GetPosition();
            Vector3 DeltaPosition = EndPosition - StartPosition;
            if (DeltaPosition.sqrMagnitude >= FlickThresholdDisSqr) {
                //デジタルに返す場合
                ////角度を判定。上が基準（0度）
                //float InputAngle = Vector3.Angle(Vector3.up, DeltaPosition);
                ////角度は鋭角で返ってくるので、xの正負で左右を判定
                //if (InputAngle < 22.5f) {
                //    //上
                //    FlickVector.x = 0;
                //    FlickVector.y = 1;
                //} else if (InputAngle < 67.5f) {
                //    //斜め上
                //    FlickVector.x = 1;
                //    FlickVector.y = 1;
                //} else if (InputAngle < 112.5f) {
                //    //横
                //    FlickVector.x = 1;
                //    FlickVector.y = 0;
                //} else if (InputAngle < 157.5f) {
                //    //斜め下
                //    FlickVector.x = 1;
                //    FlickVector.y = -1;
                //} else {
                //    //下
                //    FlickVector.x = 0;
                //    FlickVector.y = -1;
                //}

                //if (DeltaPosition.x < 0) {
                //    FlickVector.x *= -1;
                //}

                //アナログに返す場合
                FlickVector = DeltaPosition.normalized;
            }

            TouchTime = 0.0f;
            StartPosition = Vector3.zero;

            if (FlickVector != Vector3.zero) {
                isFlick = true;
            }
        } else if (TouchPhase == GodPhase.Moved) {
            //タップ中
            TouchTime += Time.deltaTime;
        }

        return isFlick;
    }
}
