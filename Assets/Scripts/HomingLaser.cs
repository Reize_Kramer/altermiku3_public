﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LineRenderer))]
public class HomingLaser : MonoBehaviour {
    LineRenderer Rend;
    int VertexCount;
    int MaxVertex;
    public int Duration;    

    List<Vector3> PathList;

    Transform tfParent;

	// Use this for initialization
	void Start () {
        Rend = GetComponent<LineRenderer>();
        tfParent = transform.parent;
        VertexCount = 1;

        //Durationは時間で指定してもらい、
        //最大頂点数にはこちらで変換する
        MaxVertex = Mathf.FloorToInt(Duration / Time.fixedDeltaTime);
        Rend.numPositions = VertexCount;
        
        PathList = new List<Vector3>();
        PathList.Add(transform.localPosition);
        
        Rend.SetPosition(VertexCount - 1, tfParent.TransformPoint(PathList[VertexCount - 1]));
	}

    void OnEnable() {
        //オブジェクトプールから再利用された時のための初期化
        if(PathList != null) {
            PathList.Clear();
            PathList.Add(transform.localPosition);

            VertexCount = 1;
            Rend.SetPosition(VertexCount - 1, tfParent.TransformPoint(PathList[VertexCount - 1]));

        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
        //最初の方の点を消していく
        if(VertexCount >= MaxVertex) {
            PathList.RemoveAt(0);
        } else {
            VertexCount++;
        }

        //現在点をリストに追加
        PathList.Add(transform.localPosition);        
    }

    void Update() {
        //描画ごとに現在地をレンダラーに登録
        Rend.numPositions = PathList.Count;

        int idx = 0;
        foreach (Vector3 Path in PathList) {
            Rend.SetPosition(idx, tfParent.TransformPoint(Path));
            idx++;
        }
        //FixedUpdateの同期ズレ対策で、最終座標のみupdateで設定してから描画する
        Rend.SetPosition(VertexCount - 1, transform.position);
    }

    public void addPoint() {
        if (VertexCount >= MaxVertex) {
            PathList.RemoveAt(0);
        } else {
            VertexCount++;
        }

        //現在点をリストに追加
        PathList.Add(transform.localPosition);
    }
}
