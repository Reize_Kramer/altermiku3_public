﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class EnemyShot : MonoBehaviour
{
    public GameObject Explosion;
    public float HitShake = 1.0f;
    Vector3 defaultPosition;
    public float LifeTime;
    public GameObject DisappearPrefab;

    void OnEnable ()
    {
        //生成後一定時間で無効化
        if (LifeTime != 0.0f) {
            StartCoroutine (SetInactive (LifeTime));
        }
    }

    // Use this for initialization
    void Start ()
    {
        OnEnable ();
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    //void OnCollisionEnter( Collision col) {
    //Debug.Log("Collision:" + col.gameObject.tag);
    //}

    void OnTriggerEnter (Collider col)
    {
        //プレイヤーに直撃したら爆発
        if (col.gameObject.tag == "Player") {
            EnemyMoveAndAttack.EnemyShotToPlayerPools.Add (gameObject);
            gameObject.SetActive (false);

            GameObject ExplosionObj = Instantiate (Explosion, transform.position, transform.rotation) as GameObject;
            ExplosionObj.transform.parent = transform.parent;
            Explosion ExplosionScript = ExplosionObj.GetComponent<Explosion> ();
            ExplosionScript.isLocalSpace = true;

            //画面を揺らす
            GameObject cameraObj = Camera.main.transform.gameObject;
            cameraObj.GetComponent<CameraVibration> ().VibrationVol = HitShake;
            cameraObj.GetComponent<ScreenOverlay> ().intensity = 2.0f;

            //被弾処理をコール
            GameObject.FindGameObjectWithTag("GameController").GetComponent<DamageController>().Damaged(1);
        }

        //バリアに直撃したら消滅
        if (col.gameObject.tag == "Barrier") {
            Disappear();
        }
    }

    //弾を無効にする
    IEnumerator SetInactive (float time)
    {
        yield return new WaitForSeconds (time);
        EnemyMoveAndAttack.EnemyShotToPlayerPools.Add (gameObject);
        gameObject.SetActive (false);
    }

    //消滅処理（バリアに接触、EnemyManagerからの消去命令、時間切れ等、
    //被弾以外の要因で消滅する場合。
    public void Disappear() {
        EnemyMoveAndAttack.EnemyShotToPlayerPools.Add(gameObject);
        gameObject.SetActive(false);

        GameObject DisappearObj = Instantiate(DisappearPrefab, transform.position, transform.rotation) as GameObject;
        DisappearObj.transform.parent = transform.parent;
        Explosion ExplosionScript = DisappearObj.GetComponent<Explosion>();
        ExplosionScript.isLocalSpace = true;

    }
}
