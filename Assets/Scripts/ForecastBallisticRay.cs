﻿using UnityEngine;
using System.Collections;

public class ForecastBallisticRay : MonoBehaviour {
    public Vector3 Target;
    public Vector3 ShotPoint;

    LineRenderer LR;

    public GameObject TargetPosObj;

    void OnEnable() {
        //予測線の表示
        LR = GetComponent<LineRenderer>();
        LR.SetPosition(0, transform.InverseTransformPoint(ShotPoint));
        LR.SetPosition(1, transform.InverseTransformPoint(Target));

        //目標地点を管理するためのオブジェクト
        //AlertMarkerの表示位置を決定するために参照される
        TargetPosObj.transform.position = Target;
    }

    // Use this for initialization
    void Start () {
        //予測線の表示
        LR.SetPosition(0, transform.InverseTransformPoint(ShotPoint));
        LR.SetPosition(1, transform.InverseTransformPoint(Target));

        //目標地点を管理するためのオブジェクト
        TargetPosObj.transform.position = Target;
    }
	
	// Update is called once per frame
	void Update () {
    }
}
