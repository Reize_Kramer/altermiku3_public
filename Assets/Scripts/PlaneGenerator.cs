﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaneGenerator : MonoBehaviour {
    public GameObject prefab;
    public int PlanesNum; //生成しておくPlaneの数
    List<GameObject> planes = new List<GameObject>();
    Vector3 prefabSize;
    float unitLength;
    int planeIndex = 0;

    public float ScrollSpeed;
    public float ScrollSpeedTarget;
    //float ScrollCount = 0.0f;

    GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");

        //プレファブの大きさを取得（プレファブからはサイズが測れないので、先に作成してしまう）
        GameObject CurrentPlane = Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
        CurrentPlane.transform.parent = transform;
        planes.Add(CurrentPlane);

        Mesh prefabMesh = CurrentPlane.GetComponent<MeshFilter>().mesh;
        Bounds prefabBounds = prefabMesh.bounds;
        
        prefabSize = prefabBounds.size;
        unitLength = prefabSize.z * CurrentPlane.transform.localScale.z;

        //planeの生成
        for (int i = 1; i < PlanesNum; i++) {
            Vector3 GeneratePos = new Vector3(transform.position.x, transform.position.y, unitLength * i);
            CurrentPlane = Instantiate(prefab, GeneratePos, Quaternion.identity) as GameObject;
            CurrentPlane.transform.parent = transform;
            planes.Add(CurrentPlane);                        
        }
	}
	
	// Update is called once per frame
	void Update () {
        //プレイヤーの位置からplainのあるべき位置を判定する
        int iPlayerPositionIdx = Mathf.FloorToInt((player.transform.position.z - transform.position.z) / unitLength);
        //再配置
        if(iPlayerPositionIdx > planeIndex) {
            GameObject ReplacePlane = planes[0];
            ReplacePlane.transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                transform.position.z + unitLength * (planeIndex + PlanesNum));
            planes.RemoveAt(0);
            planes.Add(ReplacePlane);
            planeIndex++;
        }   

        //スクロールする場合
        if(ScrollSpeed > 0) {
            planeMove();
        }

        //目標速度目指してスピードを調整する（目標速度は他のスクリプトからいじられる
        ScrollSpeed = Mathf.Lerp(ScrollSpeed, ScrollSpeedTarget, Time.deltaTime * 2);
    }

    //plain自体を動かしてスピード感を増したい場合
    void planeMove() {
        //この生成オブジェクト自体を動かす（すべてのplainは子になっている）
        transform.Translate(new Vector3(0.0f, 0.0f, ScrollSpeed * Time.deltaTime * -1));
        //ScrollCount += ScrollSpeed * Time.deltaTime;

        //if(ScrollCount > unitLength) {
        //    planeIndex--;
        //    ScrollCount -= unitLength;
        //}
    }
}
