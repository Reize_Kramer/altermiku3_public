﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class StageClear : MonoBehaviour
{
    public bool isClear = false;
    public bool isGameOver = false;
    GameObject PlayerParent;
    GameObject EnemyManager;
    GameObject Player;
    public GameObject[] Clouds;
    GameObject GUICV;
    public Canvas StageClearCV;
    public Canvas GameOverCV;

    bool isClearProcessOnce = true;

    public GameObject ClearBGMsource;

    public GameObject PlayerDestroyedExplosion;
    public GameObject PlayerDestroyedExplosion2;
    
    // Use this for initialization
    void Start ()
    {
        Player = GameObject.FindGameObjectWithTag ("Player");
        if(Player == null) {
            Debug.LogError("Player tagged Object not found.");
        }

        EnemyManager = GameObject.FindGameObjectWithTag("EnemyManager");
        if(EnemyManager == null) {
            Debug.LogError("EnemyManager tagged object not found.");
        }

        GUICV = GameObject.FindGameObjectWithTag("GUICanvas");
        if(GUICV == null) {
            Debug.LogError("GUICV tagged object not found.");
        }

        PlayerParent = Player.transform.parent.gameObject;
        isClear = false;
    }
	
    // Update is called once per frame
    void Update ()
    {
        //クリア処理
        if (isClear == true) {
            //クリア時に一回だけ行う処理
            if (isClearProcessOnce == true) {
                //敵をすべて消去する
                foreach (Transform Enemies in EnemyManager.transform) {
                    Destroy(Enemies.gameObject);
                }

                //ステージ終了処理
                StopStageEvent();

                //一定時間後にクリア画面を出す
                StartCoroutine (DisplayClearMessage (1.0f));

                isClearProcessOnce = false;
            }
        }

        //ゲームオーバー処理
        if (isGameOver == true) {
            if (isClearProcessOnce == true) {
                //ステージ終了処理
                StopStageEvent();

                //ゲームオーバー演出
                StartCoroutine(GameOverEffect());

                isClearProcessOnce = false;
            }
        }
    }

    IEnumerator AudioFadeOut (AudioSource audioBGM, float FadeOutTime)
    {
        while (audioBGM.volume > 0) {
            audioBGM.volume -= Time.deltaTime / FadeOutTime;
            yield return null;
        }
    }

    IEnumerator DisplayClearMessage (float waitTime)
    {
        //一定時間後にクリア画面を出す
        yield return new WaitForSeconds (waitTime);

        Instantiate (StageClearCV);

        //さらに一定時間後にリトライ入力を受け付けるイベントを登録する。
        yield return new WaitForSeconds (3.0f);

        AttachEvent ();
    }

    void AttachEvent ()
    {
        //タッチで画面遷移するイベントを登録する
        InputController.TouchEnd += SceneChange;
    }

    void SceneChange (Vector3 Buf)
    {
        InputController.TouchEnd -= SceneChange;

        //クリアフラグが立っていれば次のステージへ
        //いなければ同じステージをリトライ
        int Sceneidx = SceneManager.GetActiveScene().buildIndex;
        if (isClear) {
            Sceneidx++;
            if (Sceneidx < SceneManager.sceneCountInBuildSettings) {
                SceneManager.LoadScene(Sceneidx);
            } else {
                SceneManager.LoadScene(1);
            }
        } else {
            SceneManager.LoadScene(Sceneidx);
        }
    }

    IEnumerator StageClearBGM (float time)
    {
        yield return new WaitForSeconds (time);
        Instantiate(ClearBGMsource, transform.position, Quaternion.identity);
        //audioObj.Play ();
    }

    void StopStageEvent() {
        //ステージ進行を止める関数（ステージクリア時、ゲームオーバー時共用部分）
        //敵の生成を止める
        var EnemyManagerScript = EnemyManager.GetComponent<EnemyManager>();
        EnemyManagerScript.doEnemyGenerate = false;

        //攻撃システムと回避システムを止める
        GameObject GC = GameObject.FindGameObjectWithTag("GameController");
        AttackSystem AttackSystemScript = GC.GetComponent<AttackSystem>();
        AttackSystemScript.DetachEvents();

        Debug.Log("StageClear DetachAS");

        QuickBoost QuickBoostScript = Player.GetComponent<QuickBoost>();
        QuickBoostScript.DetachEvents();

        Debug.Log("StageClear DetachQB");
        //プレイヤーの向きを中央に戻す
        AttackSystemScript.ResetLookPos();
        AttackSystemScript.PlayerLookTargetObj.transform.localPosition = Vector3.zero;

        Debug.Log("StageClear ResetLokPos");

        //BGMをフェードアウトする
        AudioSource[] audios = GC.GetComponents<AudioSource>();
        foreach (AudioSource audioBGM in audios) {
            StartCoroutine(AudioFadeOut(audioBGM, 2.0f));
        }

        //GUIを非表示に
        GUICV.GetComponentInParent<Canvas>().enabled = false;

        //オブジェクトプールリストをリセット
        if (EnemyMoveAndAttack.EnemyShotToPlayerPools != null) {
            EnemyMoveAndAttack.EnemyShotToPlayerPools = new List<GameObject>();
        }
        if (AlertMarkerGUI.AlertMarkerPools != null) {
            AlertMarkerGUI.AlertMarkerPools = new List<GameObject>();
        }
        if (AttackSystem.ShotsPool != null) {
            AttackSystem.ShotsPool = new List<GameObject>();
        }

        //ステージの動きを止める
        var StageProgressScript = PlayerParent.GetComponent<StageProgress>();
        StageProgressScript.targetSpeed = 0.0f;
        //（※モーションの修正が間に合わないため暫定処理）
        StageProgressScript.Speed = 0.0f;

        //雲のスクロールを遅くする（１面のみ）
        foreach (GameObject cloud in Clouds) {
            if (cloud != null) {
                PlaneGenerator PlaneGeneratorScript = cloud.GetComponent<PlaneGenerator>();
                PlaneGeneratorScript.ScrollSpeedTarget = 1.0f;
            }
        }
    }

    IEnumerator GameOverEffect() {
        //タイムスケールを遅くする演出
        Time.timeScale = 0.5f;

        yield return new WaitForSeconds(0.5f);

        //爆発エフェクト
        GameObject explode1 = Instantiate(PlayerDestroyedExplosion, Player.transform.position, Player.transform.rotation);

        //爆発音を鳴らす
        AudioSource AS = explode1.GetComponent<AudioSource>();
        for (int i = 0; i < 3; i++) {
            AS.PlayOneShot(AS.clip);
            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds(0.3f);

        //画面をフラッシュ
        Camera.main.GetComponent<ScreenOverlay>().intensity = 3.0f;

        //二回目の爆発エフェクトを出す
        GameObject explode2 = Instantiate(PlayerDestroyedExplosion2, Player.transform.position, Player.transform.rotation);

        //プレイヤーを非アクティブに
        Player.SetActive(false);

        //オーディオリスナーが消えてしまうので、GameControllerのオーディオリスナーを有効にする
        GetComponent<AudioListener>().enabled = true;

        //爆発音を鳴らす
        AudioSource AS2 = explode2.GetComponent<AudioSource>();
        AS2.PlayOneShot(AS2.clip);

        yield return new WaitForSeconds(1.0f);

        //タイムスケールを戻す
        Time.timeScale = 1.0f;

        //一定時間後にクリア画面を出す
        yield return new WaitForSeconds(1.0f);

        Instantiate(GameOverCV);

        //さらに一定時間後にリトライ入力を受け付けるイベントを登録する。
        yield return new WaitForSeconds(3.0f);

        AttachEvent();
        
    }

    public void GameOver() {
        //ゲームオーバー（外部から呼ばれる）
        isGameOver = true;
    }
}
