﻿using UnityEngine;
using System.Collections;

public class CameraVibration : MonoBehaviour
{
    public Vector3 DefaultPosition = Vector3.zero;
    public float VibrationVol;

    //GameObject Player;

    // Use this for initialization
    void Start ()
    {
        if (DefaultPosition == Vector3.zero) {
            DefaultPosition = transform.localPosition;
        }

        //Player = GameObject.FindGameObjectWithTag("Player") as GameObject;
    }
	
    // Update is called once per frame
    void Update ()
    {
        Vector3 targetPos = DefaultPosition;

        if (VibrationVol > 0.0f) {
            Vector3 randomPos = Vector3.zero;
            randomPos.x = Random.Range (VibrationVol * -1, VibrationVol);
            randomPos.y = Random.Range (VibrationVol * -1, VibrationVol);
            randomPos.z = Random.Range (VibrationVol * -1, VibrationVol);

            transform.localPosition = targetPos + randomPos;

            VibrationVol -= Time.deltaTime;
        } else {
            transform.localPosition = Vector3.Lerp (transform.localPosition, targetPos, Time.deltaTime * 2);
        }
    }
}
