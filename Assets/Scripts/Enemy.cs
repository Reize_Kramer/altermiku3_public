﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour,IEnemy
{
    public GameObject Explosion;
    GameObject EnemyForcesManager;
    //敵残勢力を管理するオブジェクト

    // Use this for initialization
    void Start ()
    {
        
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    public void hit ()
    {
        //弾と当たったら撃破
        Destroy (gameObject);

        //撃破エフェクト
        Instantiate (Explosion, transform.position, transform.rotation);

    }
}
