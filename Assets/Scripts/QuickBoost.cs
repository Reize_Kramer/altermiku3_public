﻿using UnityEngine;
using System.Collections;

using System;

public class QuickBoost : MonoBehaviour
{
    public float BoostDist = 3.0f;
    //ブースト移動距離
    Action Move;
    //移動管理のデリゲート
    Vector3 targetPosition;
    //ブースト移動先
    public bool isBoost;
    //ブースト中かどうかの判定
    Vector3 currentVel;
    //現在速度（SmoothDamp用）
    public float BoostSpeed = 80.0f;
    //ブースト時の移動速度
    public float BoostMoveTime = 0.1f;
    //ブースト時の目標地点までかかる時間
    public float ReturnSpeed = 1.0f;
    //戻る際の移動速度
    public float ReturnMoveTime = 1.0f;
    //戻る際のかかる時間
    public float BoostCooldownTime = 1.0f;
    //ブーストが再使用可能になる時間

    float fVel;
    //float fBrakeForce = 120.0f;
    //CharacterController cc;

    float fMovedDistance;
    bool bBoosted;
    bool bReturn;
    bool bTouching;

    AudioSource Audio;

    AttackSystem AS;
    Animator AM;

    public float XAdjust = 1;
    public float YAdjust = 1;
    public float YUnderAdjust = 1;

    // Use this for initialization
    void Start ()
    { 
        //cc = GetComponent<CharacterController> ();
        Audio = GetComponent<AudioSource> ();
        AS = FindObjectOfType<AttackSystem>();
        AM = GetComponent<Animator>();

        AttachEvents();
    }

    void OnDestroy ()
    {
        DetachEvents ();
    }
    	
    // Update is called once per frame
    void Update ()
    {
        //ブースト音の調整
        Audio.volume = Mathf.Lerp (Audio.volume, 0.0f, Time.deltaTime);
    }

    void FixedUpdate ()
    {
        if (Move != null) {
            Move ();
        }
    }

    private void returnPath ()
    {
        //復路

        //タッチ中・攻撃中は動かない（加速度もリセット）
        //初期位置まで移動
        if (AS != null && AS.isAttacking == true) {
            currentVel = Vector3.zero;
            return;
        }
            
        if (bTouching != true) {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, Vector3.zero, ref currentVel, ReturnMoveTime, ReturnSpeed);
        }
        
    }

    private void outward ()
    {
        //往路
        //目標地点まで移動
        //基準座標はローカル
        transform.localPosition = Vector3.SmoothDamp (transform.localPosition, targetPosition, ref currentVel, BoostMoveTime, BoostSpeed);

        ////目標地点に十分近づき減速したら
        //if (currentVel.sqrMagnitude < 0.2f) {
        //    Move = returnPath;
        //    isBoost = false;
        //}       
        //StartCoroutine(BoostCooldown(BoostCooldownTime));

    }

    //フリックイベント
    void Flick (Vector3 QBVector)
    {
        Vector3 moveDistance;

        //デリゲートにブースト処理を設定
        //ブースト中は受け付けない            
        if (isBoost != true) {
            AM.SetBool("QB", true);

            ////斜めの場合は縦横の移動量に２の平方根を掛けて移動量を調整
            //if (Mathf.Abs (QBVector.x) != 0 && Mathf.Abs (QBVector.y) != 0) {
            //    moveDistance = (QBVector * BoostDist * 1.5f) / 1.41f;
            //} else {
            moveDistance = QBVector * BoostDist * 1.5f;
            //}

            //縦方向の移動量を若干マイルドに
            moveDistance.y *= YAdjust;

            //現在位置に加算
            targetPosition = transform.localPosition;
            targetPosition += moveDistance;

            //画面外にすっ飛ばないようにクランプ
            //横長画面なので、縦方向は短めに、横方向は広めに
            //下方向は画面外に飛び出しやすいので、さらに短く
            targetPosition.x = Mathf.Clamp(targetPosition.x, -BoostDist * XAdjust, BoostDist * XAdjust);
            targetPosition.y = Mathf.Clamp(targetPosition.y, -BoostDist * YAdjust * YUnderAdjust, BoostDist * YAdjust);

            //モーション設定(ブレンドツリー用）
            AM.SetFloat("Horizontal", QBVector.x);
            AM.SetFloat("Vertical", QBVector.y);
            AM.SetTrigger("Boosting");

            //ブースト音を再生
            Audio.volume = 1.0f;
            //オーディオソースの都合で再生開始時間を変えてみる
            Audio.time = 0.3f;
            Audio.Play ();

            Move = outward;

            isBoost = true;


            //ブースト後の硬直
            StartCoroutine(BoostCooldown(BoostCooldownTime));
        }
    }


    //イベントアタッチメソッド
    public void AttachEvents() {
        InputController.TouchDown += OnTouchBegan;
        InputController.TouchMove += OnTouchMove;
        InputController.TouchEnd += OnTouchEnd;
        InputController.Flick += Flick;
    }

    //イベントデタッチメソッド
    public void DetachEvents() {
        InputController.TouchDown -= OnTouchBegan;
        InputController.TouchMove -= OnTouchMove;
        InputController.TouchEnd -= OnTouchEnd;
        InputController.Flick -= Flick;
    }

    void OnTouchBegan(Vector3 buf) {
        bTouching = true;

        //ブースト待機状態へ移行する
        //if (!isBoost) {
        AM.SetBool("QB", true);
        AM.SetFloat("Horizontal", 0.0f);
        //}        
    }

    void OnTouchMove(Vector3 buf) {
        //ブースト待機状態へ移行する
        //if (!isBoost) {
        AM.SetBool("QB", true);
        AM.SetFloat("Horizontal", 0.0f);
        //}
    }

    void OnTouchEnd(Vector3 buf) {
        bTouching = false;

        //ブースト待機状態を解く
        AM.SetBool("QB", false);
    }

    //ブーストの硬直管理
    IEnumerator BoostCooldown(float time) {
        yield return new WaitForSeconds(time);

        //ブースト後は移動ルーチンを戻りに切り替える
        Move = returnPath;
        
        isBoost = false;
    }

    ////関数の遅延起動用コルーチン
    //private IEnumerator DelayMethod(int waitTime, Action action) {
    //    yield return new WaitForSeconds(waitTime);
    //    action();
    //}
}
