﻿using UnityEngine;
using System.Collections;

public class BoosterFire : MonoBehaviour {
    public GameObject Shot2;
    public GameObject Wave;

    private GameObject NowShot;

    // Use this for initialization
    void Start () {
        NowShot = null;
    }
	
	// Update is called once per frame
	void Update () {
        GameObject Bullet;

        if (NowShot == null) {
            GameObject wav = (GameObject)Instantiate(Wave, this.transform.position, this.transform.rotation);
            wav.transform.Rotate(Vector3.left, 90.0f);
            wav.GetComponent<BeamWave>().col = this.GetComponent<BeamParam>().BeamColor;

            Bullet = Shot2;
            //Fire
            NowShot = (GameObject)Instantiate(Bullet, this.transform.position, this.transform.rotation);

        }

        if (NowShot != null) {
            BeamParam bp = this.GetComponent<BeamParam>();
            if (NowShot.GetComponent<BeamParam>().bGero)
                NowShot.transform.parent = transform;

            Vector3 s = new Vector3(bp.Scale, bp.Scale, bp.Scale);

            NowShot.transform.localScale = s;
            NowShot.GetComponent<BeamParam>().SetBeamParam(bp);
        }
    }

    void OnDestroy() {
        //撃ち終わり
        if (NowShot != null) {
            NowShot.GetComponent<BeamParam>().bEnd = true;
        }
    }
}
