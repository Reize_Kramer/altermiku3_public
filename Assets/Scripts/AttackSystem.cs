﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using GodTouches;

public class AttackSystem : MonoBehaviour
{
    //入力関数を総括するInputControllerクラスが必要

    GameObject LockedObj;
    //public Canvas cv;
    public GameObject MarkerPanel;
    public Image imgMarkerPrefab;
    Image imgMarker;
    public bool isAttacking;
    public bool isAttackCooldown;
    public bool isMaxLocked;
    public bool isTouching;
    public float AttackCooldownTime = 1.0f;
    Dictionary<GameObject, Image> LockedObjects;

    public GameObject shotPrefab;
    //プレイヤー弾のプレファブ

    public GameObject[] goBarrels;
    //弾の発射起点

    float flickWaitTimer;
    //フリック判定の処理待ちタイマー

    public static List<GameObject> ShotsPool;

    public Image imgLockSight;

    //QuickBoost QuickBoostScript;
    GameObject PlayerObj; //プレイヤー
    Animator AM; //プレイヤーのアニメータ
    public GameObject PlayerLookTargetObj; //プレイヤーの向きの目標とするオブジェクト
    Transform PlayerLookTargetTf; //上記オブジェクトのトランスフォーム
    Transform PlayerLookTargetDefaultTf; //上記オブジェクトのデフォルト位置を管理するトランスフォーム
    Vector3 PlayerLookTargetDefault; //上記オブジェクトのデフォルト位置

    bool LockClearFlag;

    public bool UseObjectPoolonShot = true; //オブジェクトプールを利用するかどうか

    //帯電エフェクト管理用コンポーネントたち
    public GameObject LightningSetterObj;
    LightningSetter LightningSetterScript;
    public GameObject LightningSoundObj;

    List<int> WaitingBarrels; //フリック判定待ち中に登録されたバレルのリスト

    public GameObject ChargeParticleObj;
    public GameObject ReloadParticleObj;

    string[] LightningAnimFlags = {
        "Fin1Open",
        "Fin2Open",
        "Fin3Open",
        "Fin4Open",
        "Fin5Open",
        "Fin6Open",
    };

    Vector3 LockSightScale;
    float LockSightAlpha;
    bool CantLock = false;

    // Use this for initialization
    void Start ()
    {
        //ロックオンマーカーを非表示
        imgLockSight.enabled = false;

        //プレイヤーのアニメーターを取得
        PlayerObj = GameObject.FindGameObjectWithTag("Player");
        AM = PlayerObj.GetComponent<Animator>();
        
        LockedObjects = new Dictionary<GameObject, Image> ();

        //帯電エフェクトのスクリプトを取得
        if(LightningSetterObj != null) {
            LightningSetterScript = LightningSetterObj.GetComponent<LightningSetter>();
        } else {
            Debug.Log("LightningSetter object don't Set.");
        }

        WaitingBarrels = new List<int>();

        //プレイヤー向き管理システムの初期化
        PlayerLookTargetTf = PlayerLookTargetObj.transform;
        //PlayerLookTargetDefaultTf = new GameObject().transform;
        //PlayerLookTargetDefaultTf.parent = PlayerLookTargetTf.parent;
        //PlayerLookTargetDefaultTf.localPosition = PlayerLookTargetTf.localPosition;
        PlayerLookTargetDefault = PlayerLookTargetTf.localPosition;


        //タッチダウン時のイベント
        InputController.TouchDown += TouchDown;
        //タッチムーブ時のイベント
        InputController.TouchMove += TouchMove;
        //タッチアップ時のイベント
        InputController.TouchEnd += TouchEnd;
        //フリック時のイベント
        InputController.Flick += Flick;
    }

    public void DetachEvents ()
    {
        //タッチダウン時のイベント
        InputController.TouchDown -= TouchDown;
        //タッチムーブ時のイベント
        InputController.TouchMove -= TouchMove;
        //タッチアップ時のイベント
        InputController.TouchEnd -= TouchEnd;
        //フリック時のイベント
        InputController.Flick -= Flick;
    }

    void OnDestroy ()
    {
        DetachEvents ();
    }
   
    // Update is called once per frame
    void Update ()
    {        

    }

    void LateUpdate ()
    {
        Transform tfCam = Camera.main.transform;

        //if (flickWaitTimer >= InputController.FlickThresholdTime) { //フリック判定の閾値。
            //ロックマーカーの位置更新
            MarkerUpdate (tfCam);
            
        //} else {
            flickWaitTimer += Time.deltaTime;
        //}
    }

    //攻撃処理
    private IEnumerator Attack ()
    {
        //ロックしている敵がいない場合は処理しない
        if (LockedObjects.Count == 0) {
            //プレイヤーの向きを初期状態に戻す
            PlayerLookTargetObj.transform.localPosition = PlayerLookTargetDefault;
            yield break;
        }

        //bool firstTarget = true;
        int Barrelidx = 0;
        foreach (GameObject target in LockedObjects.Keys) {
            if (target != null) {
                //発射するバレルの帯電エフェクトを切る
                if (LightningSetterScript != null) {
                    LightningSetterScript.LightningCtrObjects[Barrelidx].SetActive(false);
                }

                //オブジェクトプールリストを参照
                //非アクティブなオブジェクトがあれば利用
                //なければ作成
                GameObject shot = null;
                if (ShotsPool.Count != 0 && UseObjectPoolonShot == true) {
                    shot = ShotsPool [0];
                    ShotsPool.RemoveAt (0);

                    shot.SetActive (true);

                    //それぞれのバレルから順次発射                    
                    shot.transform.position = goBarrels[Barrelidx].transform.position;
                    shot.transform.rotation = goBarrels[Barrelidx].transform.rotation;
                    Barrelidx++;
                    if (Barrelidx >= goBarrels.Length) Barrelidx = 0;
                }

                if (shot == null) {
                    //それぞれのバレルから順次発射  
                    shot = Instantiate (shotPrefab, goBarrels[Barrelidx].transform.position, goBarrels[Barrelidx].transform.rotation) as GameObject;
                    Barrelidx++;
                    if (Barrelidx >= goBarrels.Length) Barrelidx = 0;
                    shot.transform.parent = GameObject.FindGameObjectWithTag ("Player").transform.parent;
                }

                Shot shotScript = shot.GetComponent<Shot> ();
                shotScript.target = target;

                //shot.transform.position = goBarrel.transform.position;
                //shot.transform.rotation = goBarrel.transform.rotation;

                if (!isAttacking) {
                    //モーション設定
                    AM.SetBool("Attacking", true);
                    AM.Play("Attack",0, 0.0f);

                    isAttacking = true;

                    //チャージ状態ParticleSystemを切る
                    if(ChargeParticleObj != null) {
                        ChargeParticleObj.SetActive(false);
                    }
                }

                //プレイヤーの向きを攻撃対象の敵に設定
                //if (firstTarget) {
                    PlayerLookTargetObj.transform.position = target.transform.position;
                //    firstTarget = false;
                //}

                yield return new WaitForSeconds (0.2f); //＜攻撃速度
            }
        }

        //攻撃終了
        isAttacking = false;
        isMaxLocked = false;
        AM.SetBool("Attacking", false);
        isAttackCooldown = true;
        LightningSoundObj.SetActive(false);

        //プレイヤーの向きを初期状態に戻す
        PlayerLookTargetObj.transform.localPosition = PlayerLookTargetDefault;

        //リロード状態ParticleSystemをオン
        if (ReloadParticleObj != null) {
            ReloadParticleObj.SetActive(true);
        }

        //フィンの閉鎖モーション
        foreach (string finFlags in LightningAnimFlags) {
            AM.SetBool(finFlags, false);
        }

        yield return new WaitForSeconds (AttackCooldownTime); //＜攻撃硬直
        //攻撃後はロックオンをリセットする
        LockClearFlag = true;
        isAttackCooldown = false;

        //リロード状態ParticleSystemを切る
        if (ReloadParticleObj != null) {
            ReloadParticleObj.SetActive(false);
        }
        //チャージ状態ParticleSystemをON
        if (ChargeParticleObj != null) {
            ChargeParticleObj.SetActive(true);
        }
    }

    //ロックオンマーカーの更新
    private void MarkerUpdate (Transform tfCam)
    {
        foreach (KeyValuePair<GameObject, Image> pair in LockedObjects) {
            if (LockClearFlag || pair.Key == null) {
                //ロッククリア時、もしくはオブジェクトがDestroy済みなら
                //マーカーをDestroy
                if (pair.Value != null) {
                    Destroy(pair.Value.gameObject);
                }

                //展開待ちバレルをリセット
                WaitingBarrels.Clear();
                //すでに展開しているバレルもリセット
                BarrelsReset();
            } else {
                //位置更新
                //背後にいる場合もマーカーが出てしまうので、方向をチェック
                //フリック待機中は、すでに表示しているマーカー以外はＯＮにしない
                float enemyAngle = Vector3.Angle(
                                       tfCam.forward, (pair.Key.transform.position - tfCam.position).normalized);
                if (enemyAngle > 90) {
                    pair.Value.enabled = false;
                } else {
                    if (flickWaitTimer >= InputController.FlickThresholdTime) {
                        pair.Value.enabled = true;
                    }
                }

                //pair.Value.transform.position = RectTransformUtility.WorldToScreenPoint (
                //    Camera.main, pair.Key.transform.position);

                pair.Value.transform.position = Camera.main.WorldToScreenPoint(pair.Key.transform.position);

                //マーカーをくるくるさせる
                //pair.Value.transform.Rotate(new Vector3(0.0f, 0.0f, 90 * Time.deltaTime));

                //フィンの展開モーション
                //フリック待機中は処理しない
                if (flickWaitTimer >= InputController.FlickThresholdTime) {
                    if (WaitingBarrels.Count > 0) {
                        foreach (int BarrelID in WaitingBarrels) {
                            AM.SetBool(LightningAnimFlags[BarrelID], true);
                            //帯電エフェクト
                            if (LightningSetterScript != null) {
                                LightningSetterScript.LightningCtrObjects[BarrelID].SetActive(true);
                                LightningSoundObj.SetActive(true);
                            }
                        }
                        WaitingBarrels.Clear();
                    }
                }
            }
        }

        //ロッククリア時はリストを削除
        if (LockClearFlag) {
            LockedObjects.Clear();
            LockClearFlag = false;

            //すでに展開しているバレルもリセット
            BarrelsReset();
        }
    }

    //ロック処理
    private void LockOn (Vector3 TouchPosition)
    {
        int layerNo = LayerMask.NameToLayer ("Enemy");
        int layerMask = 1 << layerNo;
        Ray lockRay = Camera.main.ScreenPointToRay (TouchPosition);        

        //攻撃硬直中・ロック上限時は処理しない
        if (isAttacking || isAttackCooldown || isMaxLocked) return;

        //エネミーに当たったらロック
        RaycastHit hit;
        if (Physics.Raycast (lockRay, out hit, 100, layerMask)) {
            if (hit.collider.gameObject.tag == "Enemy") {
                LockedObj = hit.collider.gameObject;
                //ロック済みの敵かチェック
                if (LockedObjects.ContainsKey(LockedObj) == false) {
                    //ロックしたエネミーにマーカーを設定
                    //フリック判定中は表示したくないので、設定直後は非アクティブに
                    imgMarker = Instantiate<Image>(imgMarkerPrefab);
                    imgMarker.transform.SetParent(MarkerPanel.transform, false);
                    imgMarker.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, LockedObj.transform.position);
                    imgMarker.transform.rotation = Quaternion.identity;
                    imgMarker.enabled = false;

                    //展開するバレルを登録
                    WaitingBarrels.Add(LockedObjects.Count);

                    //ロックリストに追加
                    LockedObjects.Add(LockedObj, imgMarker);

                    //ロック数上限（現状はバレル数と一致）に達したらフラグを立てる
                    if( LockedObjects.Count >= goBarrels.Length) {
                        isMaxLocked = true;
                    }
                }
            }
        }
    }

    private void TouchDown (Vector3 TouchPos)
    {
        flickWaitTimer = 0.0f;
        isTouching = true;
    }

    private void TouchMove(Vector3 TouchMove) {
        //ボタンムーブでロック
        if (isAttacking == false) {
            LockOn(TouchMove);
        }

        //ロックサイトの位置更新
        if (flickWaitTimer >= InputController.FlickThresholdTime) { //フリック判定の閾値。
            LockSightCtl(TouchMove);

            //プレイヤーの向きをタッチしている方向に向ける
            Ray lockRay = Camera.main.ScreenPointToRay(TouchMove);
            int TouchLayerMask = 1 << LayerMask.NameToLayer("PlayerLookLayer");
            RaycastHit hit;
            if (Physics.Raycast(lockRay, out hit, 100, TouchLayerMask)) {
                PlayerLookTargetObj.transform.position = hit.point;
            }
        }
    }

    private void TouchEnd (Vector3 TouchEnd)
    {
        //ボタンアップで攻撃
        if (isAttacking == false && isAttackCooldown == false) {
            StartCoroutine (Attack ());
        }

        //ロックサイトを非表示
        imgLockSight.enabled = false;

        isTouching = false;
    }

    private void Flick (Vector3 Flick)
    {
        //フリック入力された場合は、
        //すでに攻撃中でなければロックリストをリセットする
        if (isAttacking == false) {
            foreach (KeyValuePair<GameObject, Image> pair in LockedObjects) {
                if (pair.Value != null) {
                    //マーカーの削除処理
                    Destroy (pair.Value.gameObject);
                }
            }
            LockedObjects.Clear ();

            //すでに展開しているバレルもリセットする
            BarrelsReset();
        }
    }


    void LockSightCtl(Vector3 TouchPos){
        //ロックサイトのスケールとアルファをLerp
        LockSightScale = Vector3.Lerp(LockSightScale, Vector3.one, Time.deltaTime * 10);
        LockSightAlpha = Mathf.Lerp(LockSightAlpha, 1.0f, Time.deltaTime * 10);

        //ロック不可能状態時
        if (isAttacking || isAttackCooldown || isMaxLocked) {
            imgLockSight.enabled = true;
            imgLockSight.color = Color.red;
            LockSightScale = Vector3.one;
            LockSightAlpha = 1.0f;
            CantLock = true;
        } else {
            //ロック可能時
            if(imgLockSight.enabled == false || CantLock == true) {
                //初期化
                imgLockSight.enabled = true;
                imgLockSight.color = Color.green;
                LockSightScale = new Vector3(3, 3, 3);
                LockSightAlpha = 0.2f;
                CantLock = false;
            }
            
        }

        //スケールとアルファをオブジェクトに設定
        imgLockSight.rectTransform.localScale = LockSightScale;
        imgLockSight.color = new Color(imgLockSight.color.r, imgLockSight.color.g, imgLockSight.color.b, LockSightAlpha);

        //タッチしている位置にロックサイトを表示
        imgLockSight.transform.position = TouchPos;
    }

    void BarrelsReset() {
        //すべてのバレルのエフェクトを閉鎖状態に戻す
        //帯電エフェクトのカット
        foreach(var obj in LightningSetterScript.LightningCtrObjects) {
            obj.SetActive(false);
        }

        //フィンの閉鎖モーション
        foreach (string finFlags in LightningAnimFlags) {
            AM.SetBool(finFlags, false);
        }
    }

    //注視点を変更したい場合に使用（ボス出現時など）
    //攻撃時、タッチ時の処理と干渉するのでそれより優先順位を低く
    //受け取ったワールド座標にレイを飛ばして座標を決定する
    public void SetLookPos(Vector3 targetPos) {
        if ((isAttacking || isTouching) == false) {
            Ray toTargetRay = new Ray(PlayerObj.transform.position, targetPos - PlayerObj.transform.position);
            int TouchLayerMask = 1 << LayerMask.NameToLayer("PlayerLookLayer");
            RaycastHit hit;
            if (Physics.Raycast(toTargetRay, out hit, 100, TouchLayerMask)) {
                PlayerLookTargetDefault = hit.point;
            }
            //注視点を更新
            PlayerLookTargetObj.transform.position = PlayerLookTargetDefault;
        }
    }

    //注視点を初期に戻す
    public void ResetLookPos() {
        PlayerLookTargetDefault = Vector3.zero;
    }
}
