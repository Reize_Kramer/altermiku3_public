﻿using UnityEngine;
using System.Collections;
using System;

public interface IEnemyActionPattern {
    void Execute(GameObject go);
    bool EndAction(GameObject go);
}
