﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class AlertMarkerGUI : MonoBehaviour {
    Transform CameraTF;
    public float LandingTime;
    //float DestroyTime = 0.5f;
    Image ImageCom;
    public Vector3 ShotPointPos;
    public Vector3 TargetPos;
    public GameObject FBRayPrefab;
    public Image CircleGauge;
    ForecastBallisticRay FBRayScript;
    public GameObject ShotObj;

    public static List<GameObject> AlertMarkerPools;

    bool startup;

    float time = 0.0f;

    void Awake() {
        if (AlertMarkerPools == null) {
            AlertMarkerPools = new List<GameObject>();
        }

        CameraTF = Camera.main.transform;
        ImageCom = GetComponent<Image>();

        FBRayScript = FBRayPrefab.GetComponent<ForecastBallisticRay>();

        //予報線の親はEnemyManagerに
        FBRayPrefab.transform.parent = GameObject.FindGameObjectWithTag("EnemyManager").transform;

    }

    void OnEnable() {
        //後から作られたものが後に描画されるように、配置順を変える
        transform.SetAsFirstSibling();

        FBRayScript.ShotPoint = ShotPointPos;
        FBRayScript.Target = TargetPos;

        FBRayPrefab.SetActive(true);

        //色とゲージを初期化
        if (ImageCom != null) {
            Color colorSetting = ImageCom.color;
            colorSetting.g = 1.0f;
            colorSetting.r = 0.0f;
            ImageCom.color = colorSetting;
            CircleGauge.fillAmount = 0.0f;
            colorSetting.a = 0.3f;
            CircleGauge.color = colorSetting;
        }

        //スケーリングエフェクト
        transform.localScale = Vector3.one * 2f;
        transform.DOScale(1.0f, 0.5f);

        time = 0.0f;
    }

    // Use this for initialization
    void Start () {        

    }

    // Update is called once per frame
    void Update () {
        //Color colorSettingForSharedMaterial = ImageCom.material.color;
        Color colorSetting = ImageCom.color;

        //予報線がDestroyされている（ステージクリアしている）場合は、自分も爆破して処理を抜ける
        if(FBRayPrefab == null) {
            Destroy(gameObject);
            return;
        }

        //親の弾が非アクティブになっている場合は、非アクティブに
        if(ShotObj.activeInHierarchy == false) {
            SetInactive();
        }

        //位置をアップデート
        transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, FBRayScript.TargetPosObj.transform.position);
        transform.rotation = Quaternion.identity;

        //色設定
        //時間経過で点滅
        //sharedMaterialのアルファをいじって、すべてのマーカーの点滅が同期するようにする。
        //colorSettingForSharedMaterial.a = Mathf.PingPong(time * 3, 1.0f);
        //ImageCom.material.color = colorSettingForSharedMaterial;

        //着弾までの時間に応じてマーカーの色を変える
        //最初は緑、着弾直前は赤
        colorSetting.g = Mathf.Lerp(1.0f, 0.0f, time / LandingTime);
        colorSetting.r = Mathf.Lerp(0.0f, 1.0f, time / LandingTime);
        ImageCom.color = colorSetting;

        //着弾までの時間に応じてゲージの長さと色を変える
        CircleGauge.fillAmount = (time / LandingTime);
        colorSetting.a = 0.3f;
        CircleGauge.color = colorSetting;

        time += Time.deltaTime;

        if(time >= LandingTime) {
            SetInactive();
        }
	}

    void SetInactive() {
        if (AlertMarkerPools != null) {
            AlertMarkerPools.Add(gameObject);
        } else {
            Debug.Log("ObjectPoolList not Initialize : AlertMarkerGUI");
        }

        FBRayPrefab.SetActive(false);
        gameObject.SetActive(false);
    }
}
