﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class ScreenOverlayManager : MonoBehaviour
{
    ScreenOverlay SO;
    public float RecoverSpeed = 1.0f;

    // Use this for initialization
    void Start ()
    {
        SO = GetComponent<ScreenOverlay> ();	
    }
	
    // Update is called once per frame
    void Update ()
    {
        SO.intensity = Mathf.Lerp (SO.intensity, 0.0f, Time.deltaTime * RecoverSpeed);
    }
}
