﻿using UnityEngine;
using System.Collections;

public class CloudGenerator : MonoBehaviour {
    //GameObject Player;
    public float generateDistance;
    public float generateInterval;
    public GameObject CloudPrefab;
    float timer = 0.0f;

	// Use this for initialization
	void Start () {
        //Player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        //タイマー
        //のちのちコルーチンにした方がいいかも
        timer += Time.deltaTime;
        if (timer > generateInterval) {
            //プレイヤーの前方に雲を発生させる
            //Vector3 PlayerPosition = Player.transform.position;
            Vector3 CameraPosition = Camera.main.transform.position;
            //Vector3 dirGenerate = (PlayerPosition - CameraPosition).normalized;
            Vector3 dirGenerate = Camera.main.transform.forward;
            Vector3 RandomVector = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            Vector3 GeneratePosition = dirGenerate * generateDistance + CameraPosition + RandomVector;

            GameObject Cloud = (GameObject)Instantiate(CloudPrefab, GeneratePosition, Quaternion.identity);
            Destroy(Cloud, 3);

            timer = 0.0f;
        }
	}
}
