﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningController : MonoBehaviour {
    //Lightning Bolt Effectのアセットをまとめて管理するオブジェクト
    
    public GameObject StartPositionParent;
    public GameObject EndPositionParent;
    List<GameObject> StartPositions;
    List<GameObject> EndPositions;
    public GameObject LightningPrefab; //LightningBoltEffectアセットのプレファブ
    List<GameObject> LightningObjects; //生成したオブジェクト
    public int LightningNum; //ひとつのフィンに生成するLightningの数
    public float FinLength; //フィンの長さ    

    // Use this for initialization
    void Start () {
        StartPositions = new List<GameObject>();
        EndPositions = new List<GameObject>();
        LightningObjects = new List<GameObject>();

        //生成数に応じてLightningBoltEffectを生成
        for(int i = 0; i < LightningNum; i++) {
            GameObject LightningEffect = Instantiate(LightningPrefab, transform.position, transform.rotation) as GameObject;
            LightningEffect.transform.parent = transform;
            LightningObjects.Add(LightningEffect);

            //アセットのスクリプトを取得
            var LightningBoltScr = LightningEffect.GetComponent<DigitalRuby.LightningBolt.LightningBoltScript>();

            //生成数とフィンの長さから、初期位置を均等に割り当てるため計算
            Vector3 pos = Vector3.zero;
            pos.y = i * -FinLength / LightningNum;

            //StartPositionとEndPositionのオブジェクトを設定
            GameObject StartPositionObj = new GameObject();
            StartPositionObj.transform.parent = StartPositionParent.transform;
            StartPositionObj.transform.localRotation = Quaternion.identity;
            StartPositionObj.transform.localPosition = Vector3.zero;
            StartPositionObj.transform.localPosition += pos;
            LightningBoltScr.StartObject = StartPositionObj;
            StartPositions.Add(StartPositionObj);

            GameObject EndPositionObj = new GameObject();
            EndPositionObj.transform.parent = EndPositionParent.transform;
            EndPositionObj.transform.localRotation = Quaternion.identity;
            EndPositionObj.transform.localPosition = Vector3.zero;
            EndPositionObj.transform.localPosition += pos;
           LightningBoltScr.EndObject = EndPositionObj;
            EndPositions.Add(EndPositionObj);
            
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Lightningの位置更新
        //PositionUpdate(StartPositions);
        //PositionUpdate(EndPositions);

    }

    void PositionUpdate(List<GameObject> positions) {
        //時間経過でLightningの位置を移動
        //フィンの長さを超えたら0に戻る
        foreach (GameObject posObj in positions) {
            Vector3 pos = posObj.transform.localPosition;
            pos.y -= Time.deltaTime;

            if (pos.y < -FinLength) {
                pos.y += FinLength;
            }

            posObj.transform.localPosition = pos;
        }
    }
}
