﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ActiveLerpScaleX : MonoBehaviour {
    public float duration;

    void OnEnable() {
        transform.localScale = new Vector3(0.0f, 1.0f, 1.0f);
        transform.DOScaleX(1.0f, duration);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
