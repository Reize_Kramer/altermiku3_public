﻿using UnityEngine;
using System.Collections;

public class LaserTest : MonoBehaviour {
    public GameObject shot;
    public Vector3[] positions;

    float time;
	// Use this for initialization
	void Start () {
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
	    if( Input.GetButton("Fire1")) {
            if(time > 0.5f) { 
                GameObject ShotObj = Instantiate(shot, transform.position, transform.rotation) as GameObject;
                ShotObj.SetActive(true);
                ShotObj.transform.parent = transform;
                Shot_Curve ShotCurveScript = ShotObj.GetComponent<Shot_Curve>();
                for (int i = 0; i < positions.Length; i++) {
                    ShotCurveScript.Positions[i] = positions[i];
                }

                time = 0;
            } 
        }

        time += Time.deltaTime;
	}
}
