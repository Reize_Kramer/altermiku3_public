﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchToStart : MonoBehaviour {
    //点滅させる
    public float FadeTime;
    Image ImageCom;
    Color ImageColor;
    float SmoothDampVel;
    float time;
    // Use this for initialization
    void Start () {
        ImageCom = GetComponent<Image>();
        ImageColor = ImageCom.color;
        time = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        ImageColor.a = Mathf.PingPong(time / 2, 0.8f) + 0.2f;
        ImageCom.color = ImageColor;
        time += Time.deltaTime;
    }
}
