﻿using UnityEngine;
using System.Collections;

public class EmitterWithAction : MonoBehaviour {
    public GameObject prefab;
    public GameObject GO;
    ActionControl AC;

	// Use this for initialization
	void Start () {
        GO = Instantiate(prefab, transform.position, transform.rotation)as GameObject;
        GO.transform.parent = transform;

        //ActionControllがアタッチされている場合は
        //今作ったオブジェクトを渡す
        AC = GetComponent<ActionControl>();
        if( AC != null) {
            AC.ControlObj = GO;
        }
	}
	
	// Update is called once per frame
	void Update () {
	    //生成した敵が破壊されたら自身も破壊
        if(GO == null) {
            Destroy(gameObject);
        }
	}
}
