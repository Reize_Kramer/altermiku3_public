﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour {
    float time;
    bool touched;
    bool ChangeScene;
    AsyncOperation AO;
    AudioSource ASource;

    public void StartGame() {
        //DetachEvents();
        //Quitウィンドウ表示中（＝時間停止中）は処理しない
        if (Time.timeScale == 0.0f) return;

        //タイトル画面が表示されて一定時間経過するまで処理しない
        if (Time.time - time < 1.0f) return;

        //一度ボタンが押されているなら処理しない
        if (touched) return;
        touched = true;

        //1面を始める
        ASource = GetComponent<AudioSource>();
        ASource.Play();
        StartCoroutine(StartGameFlag(2.0f));        
    }

    // Use this for initialization
    void Start () {
        //InputControllerにタッチ時の処理を登録
        //タッチダウン時のイベント
        //InputController.TouchDown += TouchDown;
        ////タッチムーブ時のイベント
        //InputController.TouchMove += TouchMove;
        //タッチアップ時のイベント
        //InputController.TouchEnd += TouchEnd;
        //フリック時のイベント
        //InputController.Flick += Flick;
        time = Time.time;

        //ステージ１の読み込みを始めておく
        AO = SceneManager.LoadSceneAsync(1);
        AO.allowSceneActivation = false;
        ChangeScene = false;
        touched = false;
    }

    // Update is called once per frame
    void Update() {
    }

    public void DetachEvents() {
        //タッチダウン時のイベント
        //InputController.TouchDown -= TouchDown;
        //タッチムーブ時のイベント
        //InputController.TouchMove -= TouchMove;
        //タッチアップ時のイベント
        //InputController.TouchEnd -= TouchEnd;
        //フリック時のイベント
        //InputController.Flick -= Flick;
    }

    void OnDestroy() {
        DetachEvents();
    }

    IEnumerator StartGameFlag(float DelayTime) {
        //オーディオの再生が終わるまで待つ
        yield return new WaitForSeconds(DelayTime);
        //シーン遷移
        AO.allowSceneActivation = true;
    }
}