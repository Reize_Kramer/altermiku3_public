﻿using UnityEngine;
using System.Collections;

public class ScaleLerp : MonoBehaviour {
    //GUIの生成時に拡大縮小をする
    public Vector3 StartScale = Vector3.one;
    public float LerpRate;

    Vector3 LockSightScale;
    RectTransform rtf;

	// Use this for initialization
	void Start () {
        //初期化
        rtf = GetComponent<RectTransform>();
        LockSightScale = StartScale;

        //誤ってGUI以外にアタッチされた場合はスクリプトを自爆
        if (rtf == null) {
            Debug.Log("RectTransform compornent not found.");
            Destroy(this);
        }
    }

    void OnActive() {
        //初期化
        LockSightScale = StartScale;
    }

    // Update is called once per frame
    void Update () {
        //スケールをLerp
        LockSightScale = Vector3.Lerp(LockSightScale, Vector3.one, Time.deltaTime * LerpRate);        
        rtf.localScale = LockSightScale;
    }
}
