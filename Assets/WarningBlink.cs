﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningBlink : MonoBehaviour {
    //点滅させる
    Image ImageCom;
    Color ImageColor;
    float time;

    // Use this for initialization
    void Start()
    {
        ImageCom = GetComponent<Image>();
        ImageColor = ImageCom.color;
        time = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        ImageColor.a = Mathf.PingPong(time, 0.8f) + 0.2f;
        ImageCom.color = ImageColor;
        time += Time.deltaTime;
    }
}
