﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GodTouches;

public class LookatObject : MonoBehaviour {
    public Transform targetObj;
    public Transform lookObj;
    public float RotateTime = 0.2f;
    public float MaxRotateSpeed = 5f;
    Vector3 Vel;
    GameObject Player;
    Transform PlayerTf;

    //プレイヤーを指定したオブジェクトの方に向けるコンポーネント
    //SmoothDamp的に回転するために、注視するオブジェクトを二段構えにする
    // Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        PlayerTf = Player.transform;
	}
	
	// Update is called once per frame
	void Update () {
        //lookObjをSmoothDampでtargetObjに近づける
        lookObj.localPosition = Vector3.SmoothDamp(lookObj.localPosition, targetObj.localPosition, ref Vel, RotateTime, MaxRotateSpeed);
        //lookObjを見る
        PlayerTf.LookAt(lookObj);

        //旧コード
        //徐々にターゲット方向に向く
        //Quaternion toTargetRotation = Quaternion.LookRotation(target.transform.position - Player.transform.position);
        //Player.transform.rotation = Quaternion.Slerp(Player.transform.rotation, toTargetRotation, Time.deltaTime * RotateSpeed);
    }
}
