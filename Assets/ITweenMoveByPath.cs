﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ITweenMoveByPath : MonoBehaviour {
    public string Pathname;

    public enum EaseType {
        easeInQuad,
        easeOutQuad,
        easeInOutQuad,
        easeInCubic,
        easeOutCubic,
        easeInOutCubic,
        easeInQuart,
        easeOutQuart,
        easeInOutQuart,
        easeInQuint,
        easeOutQuint,
        easeInOutQuint,
        easeInSine,
        easeOutSine,
        easeInOutSine,
        easeInExpo,
        easeOutExpo,
        easeInOutExpo,
        easeInCirc,
        easeOutCirc,
        easeInOutCirc,
        linear,
        spring,
        /* GFX47 MOD START */
        //bounce,
        easeInBounce,
        easeOutBounce,
        easeInOutBounce,
        /* GFX47 MOD END */
        easeInBack,
        easeOutBack,
        easeInOutBack,
        /* GFX47 MOD START */
        //elastic,
        easeInElastic,
        easeOutElastic,
        easeInOutElastic,
        /* GFX47 MOD END */
        punch
    }
    public EaseType easetype;

    /// <summary>
    /// The type of loop (if any) to use.  
    /// </summary>
    public enum LoopType {
        /// <summary>
        /// Do not loop.
        /// </summary>
        none,
        /// <summary>
        /// Rewind and replay.
        /// </summary>
        loop,
        /// <summary>
        /// Ping pong the animation back and forth.
        /// </summary>
        pingPong
    }

    public LoopType looptype;

    List<Vector3> nodes;
    public float time;

    public Vector3[] manualPositions;
	// Use this for initialization
	void Start () {
        iTweenPath Path;
        if (iTweenPath.paths.TryGetValue(Pathname, out Path) == false) {
            Debug.Log("ITweenPath not found:" + Pathname);
        } else {
            //nodes = new List<Vector3>(Path.nodes);

            //for(int i = 0; i < nodes.Count; i++) {
            //    nodes[i] = nodes[i] + transform.position;
            //}

            //移動命令
            //Vector3[] paths = Path.nodes.ToArray();
            //iTween.MoveTo(GetComponent<EmitterWithITween>().GO, iTween.Hash("path", paths, "islocal", true, "time", time, "easetype", easetype));
            //iTween.MoveTo(gameObject, iTween.Hash("path", paths, "islocal", true, "time", time, "easetype", easetype));

            //テストコード
            //iTween.MoveTo(GetComponent<EmitterWithITween>().GO, iTween.Hash("position", new Vector3(10.0f, 0,0), "islocal", true, "time", time, "easetype", easetype));
            
            iTween.MoveTo(GetComponent<EmitterWithAction>().GO, iTween.Hash("path", manualPositions, "islocal", true, "time", time, "easetype", easetype.ToString(), "loop", looptype.ToString()));
            GetComponent<EmitterWithAction>().GO.transform.localPosition = manualPositions[0];
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
