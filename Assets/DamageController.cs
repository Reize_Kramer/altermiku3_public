﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageController : MonoBehaviour {
    public bool isInvincibleState;
    public float InvincibleDuration;
    public GameObject BarrierPrefab; //バリアのプレファブ
    GameObject BarrierObj; //バリアのインスタンス
    Transform PlayerTf;

    EnemyManager EnemyManagerCom;

    public float PlayerAP;
    public float PlayerAPVisual;

    public Slider PlayerAPGauge;

	// Use this for initialization
	void Start () {
        //プレイヤーのトランスフォームを取得
        PlayerTf = GameObject.FindGameObjectWithTag("Player").transform;
        if (PlayerTf == null) Debug.LogError("Player tagged object not found.");

        //EnemyManagerを取得
        EnemyManagerCom = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();
        if (EnemyManagerCom == null) Debug.LogError("EnemyManager tagged object not found.");

        //バリアを生成
        BarrierObj = Instantiate(BarrierPrefab, PlayerTf.position, PlayerTf.rotation);
        BarrierObj.transform.SetParent(PlayerTf);

        if(PlayerAPGauge == null) {
            Debug.LogError("PlayerAPGauge not found.");
        } else {
            PlayerAPGauge.maxValue = PlayerAP;
            PlayerAPGauge.value = PlayerAP;
            PlayerAPVisual = PlayerAP;
        }        
    }
	
	// Update is called once per frame
	void Update () {
		//無敵モードでない場合、バリアを切る
        if(isInvincibleState == false) {
            BarrierObj.SetActive(false);
        }

        if(PlayerAPGauge != null) {
            PlayerAPGaugeControl();
        }
	}

    public void Damaged (int damageValue) {
        //被弾時の処理。引数はダメージ量（現状は１で固定）

        //無敵中は処理しない
        if (isInvincibleState) return;

        //APを減らす
        PlayerAP -= damageValue * 33.0f;

        //APが0以下ならゲームオーバー処理を行う
        if (PlayerAP < 0) {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<StageClear>().GameOver();

            //ゲームオーバー時は以下の処理は行わず戻る
            return;
        }


        //無敵状態をＯＮに
        isInvincibleState = true;

        //画面上の敵弾をリセットする
        //EnemyManagerCom.EnemyShotsReset();

        //バリアを展開
        BarrierObj.SetActive(true);

        //一定時間後に無敵モードを解除する        
        StartCoroutine(InvincibleCount(InvincibleDuration));
    }

    IEnumerator InvincibleCount(float time) {
        //指定時間待機
        yield return new WaitForSeconds(time);

        //無敵モードを解除
        isInvincibleState = false;
    }    

    void PlayerAPGaugeControl() {
        //プレイヤーの体力の表示値を実際値に近づける
        PlayerAPVisual = Mathf.Lerp(PlayerAPVisual, PlayerAP, Time.deltaTime);

        //表示値をゲージに反映
        PlayerAPGauge.value = PlayerAPVisual;        
    }


}
