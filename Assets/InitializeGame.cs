﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InitializeGame : MonoBehaviour {
    AudioSource AS;
    public AudioClip AudioData;
	// Use this for initialization
	void Start () {
        //BGM設定
        AS = GetComponent<AudioSource>();
        AS.clip = AudioData;

        //FPS設定
        Application.targetFrameRate = 30;

        //オブジェクトプールリストの初期化
        AlertMarkerGUI.AlertMarkerPools = new List<GameObject>();
        AttackSystem.ShotsPool = new List<GameObject>();
        EnemyMoveAndAttack.EnemyShotToPlayerPools = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
