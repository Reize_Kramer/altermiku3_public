﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning : MonoBehaviour {
    GUIController GUIControllerCom;

	// Use this for initialization
	void Start () {
        //起動時にWarningのimageを表示
        GUIControllerCom = GameObject.FindGameObjectWithTag("GUICanvas").GetComponent<GUIController>();
        GUIControllerCom.ActivateWarning(true);

        //５秒後に非表示に
        StartCoroutine(WarningInactive(5));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator WarningInactive(float time)
    {
        yield return new WaitForSeconds(time);
        GUIControllerCom.ActivateWarning(false);
    }
}
