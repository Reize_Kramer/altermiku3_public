﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GUIController : MonoBehaviour {
    public GameObject EnemyArmorGauge;
    public GameObject EnemyArmorGaugeManager;
    Slider EnemyArmorGaugeCom;
    public GameObject QuitWindow;
    public GameObject Warning;

    // Use this for initialization
    void Start () {
        EnemyArmorGaugeCom = EnemyArmorGaugeManager.GetComponent<Slider>();
        if(EnemyArmorGaugeCom == null) {
            Debug.LogError("EnemyArmorGauge Slider component not attached.");
        } 
	}
	
	// Update is called once per frame
	void Update () {
        //バックキーが押されたときはQuitメニューを表示
        if (Input.GetKeyDown(KeyCode.Escape)) {
            OnMenuButton();
        }
	}

    //エネミーのＡＰゲージのアクティブ状態を切り替える
    public void ActivateEnemyArmorGauge (bool flag) {
        EnemyArmorGauge.SetActive(flag);
    }

    //エネミーのAPゲージの値を増減する
    public void AddEnemyArmorGaugeValue(int value) {
        EnemyArmorGaugeCom.value += value;
    }

    //エネミーのAPゲージの値を代入する
    public void SetEnemyArmorGaugeValue(int value) {
        EnemyArmorGaugeCom.value = value;
    }

    //エネミーのAPの最大値を代入する
    public void SetEnemyArmorGaugeMax(int value) {
        EnemyArmorGaugeCom.maxValue = value;
    }

    public void OnMenuButton() {
        //タイムスケールを停止
        Debug.Log("OnMenuButton");
        Time.timeScale = 0.0f;

        //ウィンドウを開く
        QuitWindow.transform.DOScaleY(1.0f, 0.5f).SetUpdate(true);
    }

    public void OnQuitButton() {
        //ゲームを終了
        Debug.Log("OnQuitButton");
        Application.Quit();
        return;
    }

    public void OnCancelButton() {
        //タイムスケールを戻す
        Debug.Log("OnCancelButton");
        Time.timeScale = 1.0f;

        //ウィンドウを閉じる
        QuitWindow.transform.DOScaleY(0.0f, 0.5f).SetUpdate(true);
    }

    public void ActivateWarning(bool activeFlag){
        //Warningの表示切り替え
        Warning.SetActive(activeFlag);
    }

}
