﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ShortcutKeys : EditorWindow {

    // PrefabのApply
    [MenuItem("GameObject/PrefabApply &a")]
    static void KeyRemapPrefabApply() { CommonExecuteMenuItem("GameObject/Apply Changes To Prefab"); }

    static void CommonExecuteMenuItem(string iStr) {
        EditorApplication.ExecuteMenuItem(iStr);
    }
}
