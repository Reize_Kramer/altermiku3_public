﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInTitle : MonoBehaviour {
    //タイトル画面をフェードインするためのスクリプト

    public float FadeTime;
    Image ImageCom;
    Color ImageColor;
    float SmoothDampVel;

	// Use this for initialization
	void Start () {
        ImageCom = GetComponent<Image>();
        ImageColor = ImageCom.color;
	}
	
	// Update is called once per frame
	void Update () {
        ImageColor.a = Mathf.SmoothDamp(ImageColor.a, 0.0f, ref SmoothDampVel, FadeTime);
        ImageCom.color = ImageColor;
	}
}
