﻿using UnityEngine;
using System.Collections;

public class SkyboxRotate : MonoBehaviour {
    public Vector3 StartRotation;
    public Vector3 Rotation;
	// Use this for initialization

	void Start () {
        transform.rotation = Quaternion.Euler(StartRotation);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Rotation * Time.deltaTime);
	}
}
