﻿using UnityEngine;
using System.Collections;
using System;

public class ChangeBGM : MonoBehaviour
{
    GameObject GC;
    AudioSource ASBGM;
    public AudioClip Audiodata;
    public float StartTime;

    // Use this for initialization
    void Start ()
    {
        //GameControllerのAudioSourceを取得し、Clipを差し替える
        GC = GameObject.FindGameObjectWithTag ("GameController");
        ASBGM = GC.GetComponent (typeof(AudioSource)) as AudioSource;
        ASBGM.clip = Audiodata;
        ASBGM.time = StartTime;
        ASBGM.Play ();
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }
}
