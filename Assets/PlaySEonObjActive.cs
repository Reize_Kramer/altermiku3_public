﻿using UnityEngine;
using System.Collections;

public class PlaySEonObjActive : MonoBehaviour {
    //オブジェクト生成時には音を鳴らしたくないが、
    //アクティブになったときには音を鳴らしたい時用のスクリプト

    AudioSource AS;
    bool playFlag = false;

	// Use this for initialization
    void Awake () {
        AS = GetComponent<AudioSource>();
        playFlag = false;
    }

	void Start () {
        playFlag = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnEnable() {
        if(playFlag == true) {
            AS.Play();
        }
    }
}
