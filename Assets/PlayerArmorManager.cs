﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerArmorManager : MonoBehaviour {
    public Image PlayerAPGaugeFillImage;
    public Image PlayerAPGaugeFillImageBG;
    public Text GaugeLabel;
    Slider SliderCom;
    float APRatio;

    // Use this for initialization
    void Start () {
        SliderCom = GetComponent<Slider>();
        if(SliderCom == null) {
            Debug.Log("Slider Component not attached.");
        }
	}
	
	// Update is called once per frame
	void Update () {
        //APに応じてバーの色を変える
        if (SliderCom.maxValue > 0) {
            APRatio = SliderCom.value / SliderCom.maxValue;

            if(APRatio > 0.8f) {
                PlayerAPGaugeFillImage.color = Color.green;
            } else if (APRatio > 0.5f) {
                PlayerAPGaugeFillImage.color = Color.yellow;
            } else if (APRatio > 0.3f) {
                PlayerAPGaugeFillImage.color = Color.red;
            }

            //ラベルの色も変更する
            GaugeLabel.color = PlayerAPGaugeFillImage.color;

            //APがない場合はゲージ背景を点滅させる
            if (APRatio > 0.1f) {
                PlayerAPGaugeFillImageBG.color = Color.black;
            } else {
                PlayerAPGaugeFillImageBG.color = new Color(Mathf.PingPong(Time.time * 2.0f, 1.0f), 0.0f, 0.0f);
            }
        } else {
            Debug.Log("SliderCom.maxValue is Invalid value(zero).");
        }
	}
}
