﻿using UnityEngine;
using System.Collections;

public class SoundLoop : MonoBehaviour {
    //イントロが存在する素材をループさせる
    AudioSource AS;
    public float LoopEndTime; //ループの終点
    public float LoopStartTime; //ループの始点
	// Use this for initialization
	void Start () {
        AS = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(AS.time >= LoopEndTime) {
            float DifTime = AS.time - LoopEndTime;
            AS.time = LoopStartTime + DifTime;
        }
	}
}
