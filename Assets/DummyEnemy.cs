﻿using UnityEngine;
using System.Collections;

public class DummyEnemy : MonoBehaviour
{
    public float DestroyTime;

    // Use this for initialization
    void Start ()
    {
        //指定時間後に削除
        if (DestroyTime != 0) {
            Invoke ("DestroySelf", DestroyTime);
        }
    }
	
    // Update is called once per frame
    void Update ()
    {
	
    }

    void DestroySelf ()
    {
        Destroy (gameObject);
    }
}
