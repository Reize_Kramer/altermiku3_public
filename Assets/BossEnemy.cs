﻿using UnityEngine;
using System.Collections;
using System;

public class BossEnemy : MonoBehaviour, IEnemy {
    //爆発エフェクト
    public GameObject Explosion;
    public GameObject[] EnemyActionPattern;
    //AP
    public int MaxAP = 10;
    public int CurAP;

    GameObject GUIController;
    GUIController GUIControllerCom;

    AttackSystem AttackSystemCom;

    // Use this for initialization
    void Start () {
        CurAP = MaxAP;
        GUIController = GameObject.FindGameObjectWithTag("GUICanvas") as GameObject;
        if(GUIController == null){
            Debug.LogError("GUIController tagged object not found.");
        } else {
            GUIControllerCom = GUIController.GetComponent<GUIController>();

            if( GUIController == null) {
                Debug.LogError("GUIController component not attached");
            }
        }

        //登場と共に注視点の基本を自分に設定する
        AttackSystemCom = GameObject.FindGameObjectWithTag("GameController").GetComponent<AttackSystem>();
        if(AttackSystemCom != null) {
            AttackSystemCom.SetLookPos(transform.position);
        } else {
            Debug.LogError("AttackSystem not found.");
        }
    }
	
	// Update is called once per frame
	void Update () {
        //エネミーAPゲージを更新
        GUIControllerCom.SetEnemyArmorGaugeValue(CurAP);

        //注視点の基本を自分に設定する
        if (AttackSystemCom != null) {
            AttackSystemCom.SetLookPos(transform.position);
        }
    }

    public void hit() {
        //弾と当たったらダメージを加算
        CurAP--;

        //撃破処理
        if (CurAP <= 0) {
            //オブジェクトDestroy
            //凝ったイベントを作るなら専用のメソッドつくろ
            Destroy(gameObject);

            //撃破エフェクト
            Instantiate(Explosion, transform.position, transform.rotation);

            //ＡＰゲージを非表示に
            GUIControllerCom.ActivateEnemyArmorGauge(false);
        }
    }
}
