﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningSetter : MonoBehaviour {
    //Lightning Bolt Effectのアセットをフィンに適用するためのアセット

    public GameObject[] SetFins; //適用するフィンの指定
    public GameObject LightningControllerPrefab; //LightningBoltEffectの制御用オブジェクトのプレファブ
    public GameObject PositionParentPrefab;
    public Vector3 AdjustRotate; //フィンの座標軸を扱いやすく調整するための回転
    public List<GameObject> LightningCtrObjects;
    public bool OnStartActivate; //生成時にエフェクトをアクティブにするか非アクティブにするか

	// Use this for initialization
	void Start () {
        LightningCtrObjects = new List<GameObject>();

	    //フィンに管理用のオブジェクトを生成
        foreach(GameObject Fin in SetFins) {
            GameObject LightningCtr = Instantiate(
                    LightningControllerPrefab, Fin.transform.position, Fin.transform.rotation) as GameObject;
            LightningCtr.transform.parent = transform;
            LightningController CtrScript = LightningCtr.GetComponent<LightningController>();

            int idx = 0;
            //一つ目のフィンはスタート、二つ目のフィンはエンドの基準点として設定
            foreach (Transform FinWings in Fin.transform) {
                GameObject PositionParent = Instantiate(
                    PositionParentPrefab, FinWings.position, FinWings.rotation) as GameObject;
                PositionParent.transform.parent = FinWings.transform;

                //Y軸がフィンの向きと一致するように回転を設定
                PositionParent.transform.localEulerAngles = AdjustRotate;

                if (idx == 0) {
                    CtrScript.StartPositionParent = PositionParent;
                } else {
                    CtrScript.EndPositionParent = PositionParent;
                    break;
                }
                idx++;
            }

            //オブジェクトリストに登録
            LightningCtrObjects.Add(LightningCtr);

            //初期状態のアクティブ設定
            if (OnStartActivate == false) {
                LightningCtr.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
