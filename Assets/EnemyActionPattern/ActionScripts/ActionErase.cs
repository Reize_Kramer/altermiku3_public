﻿using UnityEngine;
using System.Collections;
using System;

public class ActionErase : MonoBehaviour,IEnemyActionPattern {
    //エネミーを削除したい場合のアクション（行動を完了して画面外に出た際など）
    bool EndFlag;

    public bool EndAction(GameObject go) {
        return EndFlag;
    }

    public void Execute(GameObject go) {
        Destroy(go);
        EndFlag = true;
    }

    // Use this for initialization
    void Start () {
        EndFlag = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
