﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBossGaugeInit : MonoBehaviour, IEnemyActionPattern {
    bool Complete;

    public bool EndAction(GameObject go) {
        return Complete;
    }

    public void Execute(GameObject go) {
        Debug.Log("EnemyArmorGaugeInit Executed");
        GameObject GUICanvas = GameObject.FindGameObjectWithTag("GUICanvas");
        if (GUICanvas == null) {
            Debug.LogError("GUICanvas tagged object not found.");
        } else {
            GUIController GUIControllerScript = GUICanvas.GetComponent<GUIController>();
            if(GUIControllerScript == null) {
                Debug.LogError("GUIController component not attached.");
            } else {
                GUIControllerScript.ActivateEnemyArmorGauge(true);

                //ゲージにボスのＨＰの初期値を設定
                BossEnemy BossEnemyCom = go.GetComponent<BossEnemy>();
                if (BossEnemyCom == null) {
                    Debug.LogError("BossEnemy component not attached.");
                } else {
                    GUIControllerScript.SetEnemyArmorGaugeMax(BossEnemyCom.MaxAP);
                    GUIControllerScript.SetEnemyArmorGaugeValue(BossEnemyCom.CurAP);
                }
            }
        }

        Complete = true;
    }

    // Use this for initialization
    void Start () {
        Complete = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
