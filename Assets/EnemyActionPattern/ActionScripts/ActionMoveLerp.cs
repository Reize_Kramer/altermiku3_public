﻿using UnityEngine;
using System.Collections;
using System;

public class ActionMoveLerp : MonoBehaviour,IEnemyActionPattern {
    public GameObject[] Waypoints; //目標地点設定用ゲームオブジェクト

    public int LoopTimes = 0; //ループ回数の指定
    public float MoveSpeed; //移動速度 
    public GameObject SubAction; //副次行動

    int LoopCount; //カウンタ
    int WaypointIdx; //カウンタ
    bool EndActionFlag; //遷移フラグ
    IEnemyActionPattern SubActionScript; //副次行動のスクリプト

    public float EndActionMag = 80.0f;

	// Use this for initialization
	void Start () {
        WaypointIdx = 0;
        EndActionFlag = false;
        LoopCount = 0;
 
        //副次行動（攻撃等）がある場合、スクリプト取得
        if(SubAction != null) {
            SubActionScript = SubAction.GetComponent(typeof(IEnemyActionPattern)) as IEnemyActionPattern;
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void Execute(GameObject GO) {
        //目標地点に移動
        GO.transform.position = Vector3.Lerp(GO.transform.position, Waypoints[WaypointIdx].transform.position, MoveSpeed * Time.deltaTime);

        //もし目標地点に十分近づいていたなら、次の目標地点に切り替える
        if(Vector3.SqrMagnitude(GO.transform.position - Waypoints[WaypointIdx].transform.position) < EndActionMag) {
            //次の目標地点が設定されている場合
            if (WaypointIdx < Waypoints.Length - 1) {
                WaypointIdx++;
            } else if (LoopCount < LoopTimes) {
                //ループ設定がある場合
                WaypointIdx = 0;            
            } else {
                //遷移
                EndActionFlag = true;
            }
        }

        ////副次行動（攻撃等）がある場合、実行
        if (SubActionScript != null) {
            //SubActionScript.Execute(GO);
        }
    }

    public bool EndAction(GameObject go) {
        return EndActionFlag;
    }
}
