﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ActionAttackToPlayerMultiCall : ActionAttackToPlayer1 {
    //プレイヤーへ弾を撃つクラスの拡張クラス
    //複数のオブジェクトからコールされても対応できるように、
    //発射間隔管理をローカルフラグではなくハッシュテーブルで行い、
    //呼び出し元のオブジェクト毎に管理する
    Dictionary<GameObject, bool> AttackingFlag;

    public override void Start() {
        base.Start();
        AttackingFlag = new Dictionary<GameObject, bool>();
    }

    public override IEnumerator ToPlayerShot(GameObject EnemyObj) {
        yield return StartCoroutine(base.ToPlayerShot(EnemyObj));

        //攻撃完了フラグ
        AttackingFlag[EnemyObj] = false;
    }

    public override void Execute(GameObject go) {
        //呼び出し元のオブジェクト毎にハッシュをチェック
        //ハッシュがテーブルに存在し、フラグがtrueならば攻撃中なので処理を戻す
        bool CheckFlag;
        if (AttackingFlag.TryGetValue(go, out CheckFlag)) {
            if (CheckFlag) {
                return;
            } else {
                //ハッシュがすでに存在するならフラグを立てる
                AttackingFlag[go] = true;                
            }
        } else {
            //ハッシュに登録してフラグを立てる
            AttackingFlag.Add(go, true);
        }

        StartCoroutine(ToPlayerShot(go));
    }       
}
