﻿using UnityEngine;
using System.Collections;

public class ActionControl : MonoBehaviour {
    public GameObject ControlObj;
    public GameObject[] ActionObjects;
    public IEnemyActionPattern CurAction;
    public int ActionIdx = 0;

    // Use this for initialization
    void Start () {

        if (ActionObjects.Length != 0) {
            CurAction = ActionObjects[ActionIdx].GetComponent(typeof(IEnemyActionPattern)) as IEnemyActionPattern;
            if (CurAction == null) {
                Debug.Log("CurAction = null");
            }
        } else {
            CurAction = null;
        }
	}
	
	// Update is called once per frame
	void Update () {
        //操作対象が破棄されていたら自分も消す
        if (ControlObj == null) {
            Destroy(gameObject);
            return;
        };

        //アクションの実装
        //SubAction周りが突貫作業でコスト浪費している作りなので、後ほどデリゲートを使って作り直すこと
        if (CurAction != null) {
            CurAction.Execute(ControlObj);

            //CurActionにSubActionがある場合はそれも実行する
            foreach(Transform SubActionTf in ActionObjects[ActionIdx].transform)
            {
                IEnemyActionPattern SubAction = SubActionTf.gameObject.GetComponent<IEnemyActionPattern>();
                if(SubAction != null)
                {
                    SubAction.Execute(ControlObj);
                }
            }

            if (CurAction.EndAction(ControlObj)) {
                
                ActionIdx++;
                if (ActionIdx < ActionObjects.Length) {
                    CurAction = ActionObjects[ActionIdx].GetComponent(typeof(IEnemyActionPattern)) as IEnemyActionPattern;
                } else {
                    CurAction = null;
                }
            }
        }
	}
}
