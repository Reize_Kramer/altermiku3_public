﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionRotateToPlayer : MonoBehaviour, IEnemyActionPattern {
    Transform PlayerTf;
	// Use this for initialization
	void Start () {
        PlayerTf = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Execute(GameObject go) {
        //プレイヤーの方を向く
        go.transform.rotation = Quaternion.Slerp(go.transform.rotation, Quaternion.LookRotation(PlayerTf.position - go.transform.position), 0.3f);
    }
    
    public bool EndAction(GameObject go) {
        return false;
    }
}
