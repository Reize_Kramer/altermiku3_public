﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class ActionMoveByDotweenWaypoint : MonoBehaviour, IEnemyActionPattern {
    bool PlayOnce;
    public Transform[] Waypoints;
    public float time;
    public Ease easetype;
    public float delay;
    bool EndActionFlag;

    //SubAction
    public GameObject SubAction;
    IEnemyActionPattern SubActionScript;

    // Use this for initialization
    void Start () {
        PlayOnce = false;
        
        //SubActionを取得
        if(SubAction != null) {
            SubActionScript = SubAction.GetComponent<IEnemyActionPattern>();
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Execute(GameObject GO) {
        //一度だけ実行
        if (PlayOnce || GO == null) return;

        //移動命令
        //TransformをVector3配列に変換
        List<Vector3> positions = new List<Vector3>();
        foreach(Transform waypoint in Waypoints) {
            positions.Add(waypoint.localPosition);
        }
        Vector3[] paths = positions.ToArray();

        //Tweenを実行
        GO.transform.DOLocalPath(paths, time, PathType.CatmullRom)
            .SetEase(easetype)
            .SetDelay(delay)
            .OnComplete(TweenEnd);

        PlayOnce = true;

        //SubActionを実行
        if(SubActionScript != null) {
            //SubActionScript.Execute(GO);
        }
    }

    public bool EndAction(GameObject go) {
        return EndActionFlag;
    }

    public void TweenEnd() {
        EndActionFlag = true;
        Debug.Log("Destoroy");
    }
}
