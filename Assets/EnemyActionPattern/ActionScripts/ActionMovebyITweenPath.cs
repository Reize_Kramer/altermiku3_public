﻿using UnityEngine;
using System.Collections;

public class ActionMovebyITweenPath : MonoBehaviour,IEnemyActionPattern {
    public string Pathname;
    public float time;
    public enum EaseType {
        easeInQuad,
        easeOutQuad,
        easeInOutQuad,
        easeInCubic,
        easeOutCubic,
        easeInOutCubic,
        easeInQuart,
        easeOutQuart,
        easeInOutQuart,
        easeInQuint,
        easeOutQuint,
        easeInOutQuint,
        easeInSine,
        easeOutSine,
        easeInOutSine,
        easeInExpo,
        easeOutExpo,
        easeInOutExpo,
        easeInCirc,
        easeOutCirc,
        easeInOutCirc,
        linear,
        spring,
        /* GFX47 MOD START */
        //bounce,
        easeInBounce,
        easeOutBounce,
        easeInOutBounce,
        /* GFX47 MOD END */
        easeInBack,
        easeOutBack,
        easeInOutBack,
        /* GFX47 MOD START */
        //elastic,
        easeInElastic,
        easeOutElastic,
        easeInOutElastic,
        /* GFX47 MOD END */
        punch
    }
    public EaseType easetype;

    /// <summary>
    /// The type of loop (if any) to use.  
    /// </summary>
    public enum LoopType {
        /// <summary>
        /// Do not loop.
        /// </summary>
        none,
        /// <summary>
        /// Rewind and replay.
        /// </summary>
        loop,
        /// <summary>
        /// Ping pong the animation back and forth.
        /// </summary>
        pingPong
    }

    public LoopType looptype;

    bool PlayOnce;
    bool EndActionFlag;


    // Use this for initialization
    void Start() {
        PlayOnce = false;
        EndActionFlag = false;
    }

    // Update is called once per frame
    void Update() {

    }

    public void Execute(GameObject GO) {
        //一度だけ実行
        if (PlayOnce) return;

        iTweenPath Path;
        if (iTweenPath.paths.TryGetValue(Pathname, out Path) == false) {
            Debug.Log("ITweenPath not found:" + Pathname);
            foreach (string str in iTweenPath.paths.Keys) {
                Debug.Log("ITweenPathlist:" + str);
            }
        } else {
            //移動命令
            Vector3[] paths = Path.nodes.ToArray();
            iTween.MoveTo(GO, iTween.Hash(
                "path", paths,
                "islocal", true,
                "time", time,
                "easetype", easetype.ToString(),
                "looptype", looptype.ToString(),
                "oncomplete", "TweenEnd",
                "oncompletetarget", gameObject));
            Debug.Log("iTween:moveto execute");
            PlayOnce = true;
        }
    }

    public bool EndAction(GameObject go) {
        return EndActionFlag;
    }

    public void TweenEnd() {
        EndActionFlag = true;
    }
}
