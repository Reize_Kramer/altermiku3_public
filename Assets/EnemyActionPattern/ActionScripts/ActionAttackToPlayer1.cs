﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ActionAttackToPlayer1 : MonoBehaviour,IEnemyActionPattern {
    public GameObject Shot; //ショットオブジェクト
    GameObject PlayerCenter; //プレイヤーの目標
    public float Sec = 3.0f; //弾の着弾時間
    public float ShotInterval = 5.0f; //発射間隔
    public float Delay; //発射開始までの待機時間

    public bool Attacked; //コルーチンの始動フラグ
    Coroutine GeneratedCoroutine; //生成したコルーチン

    

    public GameObject AlertMerkerPrefab; //アラートマーカー

    GameObject EnemyManagerObj;

    //SubAction
    public GameObject SubAction;
    IEnemyActionPattern SubActionScript;

    // Use this for initialization
    public virtual void Start () {
        PlayerCenter = GameObject.FindGameObjectWithTag("PlayerCenter");
        EnemyManagerObj = GameObject.FindGameObjectWithTag("EnemyManager");
        
        if (EnemyMoveAndAttack.EnemyShotToPlayerPools == null) {
            EnemyMoveAndAttack.EnemyShotToPlayerPools = new List<GameObject>();
        }

        if (AlertMarkerGUI.AlertMarkerPools == null) {
            AlertMarkerGUI.AlertMarkerPools = new List<GameObject>();
        }

        if(Delay != 0.0f) {
            StartCoroutine(DelayCoroutine(Delay));
        }

        //SubActionを取得
        if (SubAction != null) {
            SubActionScript = SubAction.GetComponent<IEnemyActionPattern>();
        }
    }

    //発射開始を遅延するコルーチン
    IEnumerator DelayCoroutine(float delay) {
        Attacked = true;
        yield return new WaitForSeconds(delay);
        Attacked = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public virtual IEnumerator ToPlayerShot(GameObject EnemyObj) {
        Vector3 From, To; //発射地点と目標地点
        GameObject ShotObj; //生成した弾
        Vector3 shotSpeed; //弾速
        Rigidbody rb; //弾のRigidbody
        
        while (EnemyObj != null) {
            //現在位置を発射位置として取得
            From = EnemyObj.transform.position;

            //プレイヤーの現在位置を目標として取得
            To = PlayerCenter.transform.position;

            //弾速を算出
            shotSpeed = (To - From) / Sec;

            //生成
            //オブジェクトプールリストに使用済みの弾があれば利用
            //なければ生成
            if (EnemyMoveAndAttack.EnemyShotToPlayerPools.Count != 0) {
                ShotObj = EnemyMoveAndAttack.EnemyShotToPlayerPools[0];
                EnemyMoveAndAttack.EnemyShotToPlayerPools.RemoveAt(0);
                ShotObj.SetActive(true);
                ShotObj.transform.position = From;
                ShotObj.transform.rotation = EnemyObj.transform.rotation;
            } else {
                ShotObj = Instantiate(Shot, From, EnemyObj.transform.rotation) as GameObject;
            }

            ShotObj.transform.parent = EnemyManagerObj.transform;
            EnemyShot EnemyShotScript = ShotObj.GetComponent<EnemyShot>();
            EnemyShotScript.LifeTime = Sec + 2.0f;
            rb = ShotObj.GetComponent<Rigidbody>();
            rb.velocity = shotSpeed;

            //攻撃アラートマーカーを表示
            //オブジェクトプールリストに使用済みのマーカーがあれば利用
            //なければ生成
            //GameOverフラグが立っている場合は生成しない
            if (GameObject.FindGameObjectWithTag("GameController").GetComponent<StageClear>().isGameOver != true)
            {
                GameObject AlertMarkerInstance;
                if (AlertMarkerGUI.AlertMarkerPools.Count != 0)
                {
                    AlertMarkerInstance = AlertMarkerGUI.AlertMarkerPools[0];
                    AlertMarkerGUI.AlertMarkerPools.RemoveAt(0);
                }
                else
                {
                    AlertMarkerInstance = Instantiate(AlertMerkerPrefab, To, Quaternion.identity) as GameObject;
                }

                //キャンバス取得、子に設定
                GameObject MarkerCanvas = GameObject.FindGameObjectWithTag("MarkerCanvas");
                AlertMarkerInstance.transform.SetParent(MarkerCanvas.transform);
                AlertMarkerInstance.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, To);
                AlertMarkerInstance.transform.rotation = Quaternion.identity;

                //マーカーのスクリプトに値を渡す
                AlertMarkerGUI AlertMarkerScript = AlertMarkerInstance.GetComponent<AlertMarkerGUI>();
                AlertMarkerScript.LandingTime = Sec;
                AlertMarkerScript.ShotPointPos = From;
                AlertMarkerScript.TargetPos = To;
                AlertMarkerScript.ShotObj = ShotObj;
                AlertMarkerInstance.SetActive(true);
            }

            yield return new WaitForSeconds(ShotInterval);

            Attacked = false;
            yield break;
        }
    }

    public virtual void Execute(GameObject go) {
        if (Attacked == false) {
            GeneratedCoroutine = StartCoroutine(ToPlayerShot(go));
            Attacked = true;
        }

        //SubActionを実行
        if (SubActionScript != null) {
            //SubActionScript.Execute(go);
        }
    }

    public bool EndAction(GameObject go) {
        return false;
    }
    
}
