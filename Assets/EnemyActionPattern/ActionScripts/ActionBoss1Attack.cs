﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ActionBoss1Attack : MonoBehaviour,IEnemyActionPattern {
    public GameObject[] Parts; //部位
    public GameObject[] AttackPattern; //部位毎の攻撃行動
    List<IEnemyActionPattern> AttackPatternScripts;

    int PartsIdx;
    bool Attacked;

    // Use this for initialization
    void Start () {
        PartsIdx = 0;
        Attacked = false;

        //攻撃コンポーネントを取得してリストに格納
        AttackPatternScripts = new List<IEnemyActionPattern>();
        foreach(var AttackScript in AttackPattern) {
            AttackPatternScripts.Add(AttackScript.GetComponent<IEnemyActionPattern>());
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool EndAction(GameObject go) {
        return false;
    }

    public void Execute(GameObject go) {
        //部位ごとに設定された攻撃行動を行う
        for (PartsIdx = 0; PartsIdx < Parts.Length; PartsIdx++) {
            if (Parts[PartsIdx] != null) {
                AttackPatternScripts[PartsIdx].Execute(Parts[PartsIdx]);
            }
        }
    }

}
