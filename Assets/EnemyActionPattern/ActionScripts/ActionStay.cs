﻿using UnityEngine;
using System.Collections;
using System;

public class ActionStay : MonoBehaviour,IEnemyActionPattern {
    //一定時間その場に待機する行動
    public float time;
    float timeCount;
    bool EndFlag;
    bool StartFlag;

    //SubAction
    public GameObject SubAction;
    IEnemyActionPattern SubActionScript;

    public bool EndAction(GameObject go) {
        return EndFlag;
    }

    public void Execute(GameObject go) {
        if(StartFlag == false) {
            timeCount = 0.0f;
            StartFlag = true;
        }

        //SubActionを実行
        if (SubActionScript != null) {
            //SubActionScript.Execute(go);
        }
    }

    // Use this for initialization
    void Start () {
        EndFlag = false;
        StartFlag = false;

        //SubActionを取得
        if (SubAction != null) {
            SubActionScript = SubAction.GetComponent<IEnemyActionPattern>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (StartFlag) {
            timeCount += Time.deltaTime;
            if (timeCount >= time) {
                EndFlag = true;
            }
        }
	}
}
