﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class BossTarget : MonoBehaviour,IEnemy {
    //ボスオブジェクト
    public GameObject BossGO;
    IEnemy BossScript;
    public GameObject DamageEffect;
    public GameObject DestroyEffect;
    public int MaxAP;
    public int CurAP;

    public Image TargetMarkerPrefab;
    Image MarkerObj;
    GameObject MarkerCanvas;
    float MarkerDurationTime = 0.5f;
    float time;

	// Use this for initialization
	void Start () {
        //初期化
        CurAP = MaxAP;
        MarkerCanvas = GameObject.FindGameObjectWithTag("MarkerCanvas");

        //ボスオブジェクトのスクリプト取得
        BossScript = BossGO.GetComponent(typeof(IEnemy)) as IEnemy;

        //ターゲットマーカーを生成し、Canvasにセット
        MarkerObj = Instantiate(TargetMarkerPrefab) as Image;
        MarkerObj.transform.SetParent(MarkerCanvas.transform);
        MarkerObj.transform.localScale = Vector3.one;
        MarkerObj.transform.rotation = Quaternion.identity;
        MarkerObj.transform.DOScale(1.3f, 0.7f).SetLoops(-1, LoopType.Yoyo);
    }
	
	// Update is called once per frame
	void Update () {
        //マーカーを更新
        if (MarkerObj != null) {
            MarkerObj.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, transform.position);            
        }
        //テストコード
        /*
        time -= Time.deltaTime;
        if(time < 0) {
            MarkerObj.transform.localScale = Vector3.one;
        }
        */
    }

    /*
    void OnWillRenderObject() {
        //テストコード
        if (Camera.current.tag == "MainCamera") {
            Debug.Log("OnWillRenderObject");
            MarkerObj.transform.localScale = Vector3.one * 3.0f;
            time = MarkerDurationTime;
        }
    }
    */

    public void hit() {
        //ダメージエフェクト
        Instantiate(DamageEffect, transform.position, transform.rotation);

        //親オブジェクトのダメージ処理
        BossScript.hit();

        //ダメージ処理
        CurAP--;

        //撃破
        if(CurAP <= 0) {
            GameObject DestroyObj = Instantiate(DestroyEffect, transform.position, transform.rotation) as GameObject;
            DestroyObj.transform.parent = transform;

            //当たり判定を切る
            GetComponent<SphereCollider>().enabled = false;
            //メッシュを切る
            GetComponent<MeshRenderer>().enabled = false;
            //マーカーをDestroy
            Destroy(MarkerObj.gameObject);

        }
    }

    void OnDestroy() {
        //マーカーを道連れ
        if(MarkerObj != null) {
            Destroy(MarkerObj);
        }
    }
}
