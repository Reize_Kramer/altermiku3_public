﻿using UnityEngine;
using System.Collections;

public class SEFade : MonoBehaviour {
    public float FadeInEndTime = 0;   //フェードインを完了する時間

    public float Volume = 1;    //音量

    public float FadeOutStartTime = 0; //フェードアウトを開始する時間
    public float FadeOutLength; //フェードアウトにかける時間

    AudioSource AS;
    
	// Use this for initialization
	void Start () {
        AS = GetComponent<AudioSource>();
        AS.volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (FadeInEndTime != 0 && AS.time < FadeInEndTime) {
            AS.volume = AS.time / FadeInEndTime * Volume;
        } else if (FadeOutStartTime != 0 && AS.time > FadeOutStartTime){
            float FadeLength = AS.time - FadeOutStartTime;
            AS.volume = 1 - (FadeLength / FadeOutLength);
        } else {
            AS.volume = Volume;
        }
	    
	}
}
